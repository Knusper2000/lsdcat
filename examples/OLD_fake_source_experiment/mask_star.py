from astropy.io import fits
import numpy as np
import sys

hdu = fits.open(sys.argv[1],memmap=False)
data = hdu[int(sys.argv[2])].data
data[:,60:105,90:130] = 0
outfilename = 'masked_star_'+sys.argv[1]
hdu.writeto(outfilename)
print(outfilename)
