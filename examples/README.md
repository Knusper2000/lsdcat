This folder contains the following:

- `mask.fits`: Mask file to be used with the usage example in the
  appendix of the LSDCat Paper.  The mask is created as described
  in the main LSDCat README 
