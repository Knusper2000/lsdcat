#! /usr/bin/env python

__version__ = "1.0.2"

import sys, os
import argparse
import math
from random import choice
import random
from astropy.io import fits
from astropy.io import fits, ascii
import numpy as np
import matplotlib.pyplot as plt
import pylab as p

# from astLib import astWCS # http://astlib.sourceforge.net
from astropy import wcs as astWCS

from time import time

import line_em_funcs as lef

# get string of the commandline that was entered by the user
command = os.path.basename(sys.argv[0])
for entity in sys.argv[1:]:
    command = command + " " + entity

parser = argparse.ArgumentParser(
    """
Apply effective noise (effective variance) to an existing variance
datacube. 
"""
)
parser.add_argument(
    "input_cube",
    type=str,
    help="""
FITS Datacube containing a STAT extension. After effective variance
is applied, this datacube will contain an additional extension 
EFF_STAT.
""",
)
parser.add_argument(
    "effective_noise",
    type=str,
    help="""
FITS file containing the 1D effective noise spectrum
(generated with gen_noise_spec.py).
""",
)
parser.add_argument(
    "--blowup",
    action="store_true",
    help="""
By default the effective noise will rescale the noise found in the
STAT extension. Use this switch to write out a uniform cube with the
effective noise instead.
""",
)
parser.add_argument(
    "--rsexp",
    action="store_true",
    help="""
Rescale --blowup cube using exposure count HDU [HDU Name: EXP]: 
sigma'=sigma*sqrt(nmax/n)
""",
)
parser.add_argument(
    "--NHDU",
    type=int,
    default=2,
    help="""
Index of the HDU storing the variance in input_cube (zero-indexed).
 (default: 2)
""",
)
parser.add_argument(
    "--output",
    type=str,
    default="none",
    help="Name of the output file " + "(default: <input_cube>+_incl_effnoised.fits)",
)

args = parser.parse_args()
input_cube = args.input_cube
effnoisefile = args.effective_noise
nhdu = args.NHDU  # HDU is expected to be provided 0-indexed by
# the user

print(
    "apply_eff_noise.py - reading input cube from "
    + input_cube
    + " (HDU "
    + str(args.NHDU)
    + ")"
)
noise_cube_hdu = fits.open(input_cube)
noise_cube = noise_cube_hdu[args.NHDU].data
noise_header = noise_cube_hdu[args.NHDU].header

print("apply_eff_noise.py - reading effective variance from " + effnoisefile)
effvar_hdu = fits.open(effnoisefile)
effvar = effvar_hdu["PRIMARY"].data
effvar_header = effvar_hdu["PRIMARY"].header

try:
    effvar_header["ENR"] == "gen_noise_spec.py"
except:
    print(
        """ WARNING: Expecting effective noise spectrum from
    gen_noise_spec.py, but header keywords from that script are
    not present.  This can break things."""
    )
# assert effvar_header['ENR'] == 'gen_noise_spec.py',\
#     'Expecting effective noise spectrum from gen_noise_spec.py. '+\
#     'Some rewriting would be neccesary for a more general approach.'
ppvar_avg = effvar_hdu["PPVAR_AVG"].data
ppvar_avg_header = effvar_hdu["PPVAR_AVG"].header

assert effvar.shape[0] == noise_cube.shape[0]  # dimensionality must
# be the same!

new_noise_header = noise_header.copy()
new_noise_header["EXTNAME"] = "EFF_STAT"

if args.blowup:
    # blowing up the variance to a cube
    print("apply_eff_noise.py - creating noisecube from " + "effective noise ...")
    # nans in the old variance cube are ignored
    nans = np.isnan(np.sum(noise_cube, axis=0))

    effvar_ratio = effvar / ppvar_avg
    effvar_ratio[np.isnan(effvar_ratio)] = 1.0

    # handbrake... if ratio falls below one, we take the average of
    # the pipeline propagated variance instead.
    effvar[effvar_ratio <= 1.0] = ppvar_avg[effvar_ratio <= 1.0]

    for i in range(len(effvar)):
        noise_cube[i, ~nans] = effvar[i]

    if args.rsexp:
        print(
            "apply_eff_noise.py - rescaling effective variance "
            + "using exposure count HDU ..."
        )
        try:
            exp_hdu = noise_cube_hdu["EXP"]
        except KeyError:
            print(
                "ERROR: Input FITS file "
                + input_cube
                + " does not contain EXP HDU... ABORT!"
            )
            sys.exit(2)

        exp_cube = exp_hdu.data  # n
        exp_cube /= exp_cube.max()  # n / n_max
        # sigma'^2 = sigma^2 * (n_max / n)
        noise_cube[exp_cube > 0] /= exp_cube[exp_cube > 0]

    new_noise_header["COMMENT"] = (
        "EFFECTIVE NOISE - PIPELINE " + "PROPAGATED AVG NOISE USED"
    )
else:
    # rescaling every voxel
    print("apply_eff_noise.py - calculating rescaling factor for " + "every voxel ...")
    scale_cube = effvar[:, np.newaxis, np.newaxis] / noise_cube
    select = scale_cube > 1 & ~np.isnan(scale_cube)
    print("apply_eff_noise.py - rescaling noise ...")
    noise_cube[select] *= scale_cube[select]
    new_noise_header["COMMENT"] = (
        "PIPELINE PROPAGATED NOISE " + "RESCALED WITH EFFECTIVE NOISE"
    )


assert input_cube[-5:] == ".fits"
if args.output == "none":
    output_cube = input_cube[:-5] + "_incl_effnoised.fits"
else:
    output_cube = args.output
    assert output_cube[-5:] == ".fits"

print(
    "apply_eff_noise.py - Appending effective noise HDU to "
    + input_cube
    + " and writing new FITS file "
    + output_cube
    + " .."
)
cube_hdulist = fits.open(input_cube)

# Put header info from gen_noise_spec.py AND apply_eff_noise.py (us)
# in the resulting header(s)
header0 = cube_hdulist[0].header
for key in effvar_header:
    if "ENR" in key:
        header0[key] = effvar_header[key]
        header0.comments[key] = effvar_header.comments[key]
        new_noise_header[key] = effvar_header[key]
        new_noise_header.comments[key] = effvar_header.comments[key]

header0["ANR"] = (os.path.basename(sys.argv[0]), "effective noise application routine.")
header0["ANRV"] = (__version__, "ANR version")
header0["HISTORY"] = (command, "ANR full command")
header0["ANRIN"] = (input_cube, "ANR input FITS file")
header0["ANRINN"] = (nhdu, "ANRIN pipeline propagated variance HDU")
header0["ANRNF"] = (effnoisefile, "ANR effective noise file")
header0["ANREFHDU"] = (len(noise_cube_hdu), "Effective Noise Cube HDU (here)")
header0["ANREFM"] = (
    "blowup" if args.blowup else "rescaled",
    "ANR application method - blowup or rescaled",
)
for key in header0:
    if "ANR" in key:
        new_noise_header[key] = header0[key]
        new_noise_header.comments[key] = header0.comments[key]

# appending effective noise to HDU list of input cube and write new
# FITS file to disk
cube_hdulist.append(fits.ImageHDU(data=noise_cube, header=new_noise_header))
cube_hdulist.writeto(output_cube)

print("apply_eff_noise.py - All Done!")
