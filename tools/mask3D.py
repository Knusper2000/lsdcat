#! /usr/bin/env python
# FILE: mask3D.py
# AUTHOR: C. Herenz
# DATE: 2011-07
# INFO: Create a 3D mask from 2D masks including mask evolution,
#       (1st HDU is a cube of the mask - not very memory convinient, but
#       convinient for further masking out in the cube, 2nd HDU is just flat 2D
#       mask, 3rd HDU contains the original mask before evolution (evolution desc. in
#       my Master-Thesis))

import sys
import getopt
from astropy.io import fits
import pylab as p

sys.path.append("../lib/")
import masklib


def usage():
    # print out usage message
    sys.stdout.write(
        """mask3D.py - converts 2D mask into 3D mask cube 

Usage: mask3D.py <infile> <HDU> <cycles> <outfile>

<infile> = Filename of FITS File containing 2D mask
<HDU> = HDU containing the 2D mask
<cycles> = No. of cylces for mask evolution
<outfile> name of the outfile

(mask3D.py --help or mask3D.py -h prints this message)

"""
    )


def set_vars():
    """
    set variables for mask3D.py
    infile,hdu,cycles,outfile = set_vars()
    """
    if len(sys.argv) == 1:
        print("ERROR: No input arguments provided")
        print("for mandatory options, see usage message below:\n")
        usage()
        sys.exit(2)
    if sys.argv[1] == "-h" or sys.argv[1] == "--help":
        usage()
        sys.exit(0)
    if len(sys.argv) < 4:
        print("Error: unrecognized options! See following usage message:")
        usage()
        sys.exit(2)
    infile = sys.argv[1]
    hdu = int(sys.argv[2]) - 1
    cycles = int(sys.argv[3])
    outfile = str(sys.argv[4])
    return infile, hdu, cycles, outfile


infile, hdu, cycles, outfile = set_vars()
print(
    "mask3d.py: Making 3D mask from "
    + str(infile)
    + " (HDU:"
    + str(hdu)
    + ", evolving "
    + str(cycles)
    + " cycles)"
)
mask2dhdu = fits.open(infile)
mask2d = mask2dhdu[hdu].data
oldmask = mask2d.copy()
mask2dhead = mask2dhdu[hdu].header

if cycles > 0:
    masklib.evolve(cycles, mask2d)

print(str(infile) + ": Creating the filter cube (this takes some time..)")
spec_dim = 3463  # assuming typical MUSE spectral dimension here
spec_range = p.ones(spec_dim)
# the next lines stacks all  the 2d arrays to a 3d cube of spectral dimension
mask2d = (
    mask2d.transpose()
)  # <- remember to transpose back if this array needs to be used again
filter_cube = p.array(
    p.dstack(p.tensordot(spec_range, mask2d, axes=0)), dtype=p.int16
)  # !
head1 = mask2dhead.copy()
head2 = mask2dhead.copy()
head3 = mask2dhead.copy()
mask2d = mask2d.transpose()  # << transposing map back
filter_cube = filter_cube.transpose()  # << NAXIS3,NAXIS2,NAXIS1
# preparing Header 1 (MASK 3D-CUBE):
head1.update("EXTNAME", "MASK3D")
head1.update("NAXIS3", 3463, after="NAXIS2")
head1.update("CRPIX3", 1, comment="Start Wavelength in pixel", after="CRPIX2")
head1.update("CRVAL3", 480.0, comment="Start Wavelength", after="CRVAL2")
head1.update("CTYPE3", "AWAV", after="CTYPE2")
head1.update("CUNIT3", "nm", comment="Wavelength units", after="CUNIT2")
head1.update("CD1_3", 0)
head1.update("CD2_3", 0)
head1.update("CD3_2", 0)
head1.update("CD3_1", 0)
head1.update("CD3_3", 1)
primaryHDU = fits.PrimaryHDU(data=filter_cube.astype(p.int16), header=head1)
secondHDU = fits.ImageHDU(data=mask2d.astype(p.int16), header=head2)
head3.update("EXTNAME", "MASK2D_ORIG")
thirdHDU = fits.ImageHDU(data=oldmask.astype(p.int16), header=head3)
hdulist = fits.HDUList([primaryHDU, secondHDU, thirdHDU])
hdulist.writeto(outfile, overwrite=True)
print(infile + ": mask3D.py finished - written outfile " + str(outfile))
