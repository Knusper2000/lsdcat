#! /usr/bin/env python
#
# FILE:    cut_cat_lead_line.py
# AUTHORS: Tanya Urrutia
# DESCR.:  Cut an LSD-Cat catalog to only include the lead-emission line
#          This is the one with the highest signal-to-noise
#

import argparse
import numpy as np
from astropy.io import fits
from astropy.table import Table

parser = argparse.ArgumentParser(
    description="""Cut an LSD-Cat catalog to only include the lead-emission line. This is the one with the highest signal-to-noise."""
)


parser.add_argument(
    "-i",
    "--input",
    required=True,
    type=str,
    help="Name of the LSDCat FITS catalog. It can be the output of lsd_cat.py or lsd_cat_measure.py, but it needs to have an object ID number (current default: 'ID') to which the catalog assigns the emission lines and an attribute to sort by (currently signal to noise DETSN_MAX)",
)

parser.add_argument(
    "-o",
    "--output",
    type=str,
    default=None,
    help="Name of the output culled fits table. [Default: INPUT_cut.fits",
)

parser.add_argument(
    "--id",
    type=str,
    default="ID",
    help="Column denoting the object ID. [Default: 'ID']",
)

parser.add_argument(
    "--sn",
    type=str,
    default="DETSN_MAX",
    help="Column denoting the maximum parameter for which we shold sort. [Default: 'DETSN_MAX']",
)

args = parser.parse_args()
input_filename = args.input
if args.output == None:
    out_filename = input_filename[:-5] + "_cut.fits"
else:
    out_filename = args.output
ident = args.id
s2n = args.sn

cat = Table(fits.getdata(input_filename))
ID = cat[ident]
DETSN_MAX = cat[s2n]
rrow = list(range(len(cat)))

for obj in range(1, max(ID) + 1):
    if obj in ID:
        sn_obj = DETSN_MAX[ID == obj]
        index = np.where((DETSN_MAX == max(sn_obj)) & (ID == obj))[0][0]
        rrow.remove(index)

cat.remove_rows(rrow)
cat.write(out_filename)
print("Done cutting %s to %s" % (input_filename, out_filename))
