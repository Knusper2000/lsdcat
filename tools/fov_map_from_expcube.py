#!/usr/bin/env python
##
# FILE: fov_map_from_expcube.py
# AUTHOR: C. Herenz (2015)
# DESCR.: Create map of number of exposed voxels within FoV
#         (part of LSDCat suite)


from lsd_cat_lib import get_version

__version__ = get_version()
from line_em_funcs import int_or_str, hierarch_multi_line
import spatial_smooth_lib

import sys, os
from datetime import datetime
import argparse
import multiprocessing

import numpy as np
from astropy.io import fits

now = datetime.now()

# workaround to allow for negative numbers not being treated as command-line arguments
# https://stackoverflow.com/a/21446783/2275260
for i, arg in enumerate(sys.argv):
    if (arg[0] == "-") and arg[1].isdigit():
        sys.argv[i] = " " + arg

parser = argparse.ArgumentParser(
    description=""" 
 Creates from an exposure map datacube an exposure map image.  An
 exposure map datacube contains in every voxel the number of exposures
 that went into this voxel, while an exposure map image contains the
 number of exposure for each spatial pixel.  Such a map can be used,
 e.g., to identify regions without any exposures (e.g. field borders).
 Additionally, the exposure cube can be spatially smoothed, which is usefull
 if the output is to be used as an exposure map for lsd_cc_spectral.py.
 """
)
parser.add_argument("input", type=str, help="""Input FITS cube""")
parser.add_argument(
    "-E",
    "--exphdu",
    type=int_or_str,
    default="3",
    help=""" 
FITS HDU name or number (0-indexed) that contains the exposure
cube.""",
)
parser.add_argument(
    "--func",
    type=str,
    default="sum",
    help=""" 
                    Function used to compute the exposure map image.  Can be either `sum`
                    (default), `mean`, `median`, or 'max'. """,
)
parser.add_argument(
    "--filter",
    type=str,
    default="none",
    help="""
The filter used for spatial smoothing, can be either 'none' (default), 'gauss', or 'moffat'.
""",
)
parser.add_argument(
    "-bc",
    default=["3.5"],
    nargs="*",
    help="""
                    List of polynomial coefficients (whitespace seperated) for the beta parameter of
                    the Moffat profile, i.e. -bc b0 b1 b2 .. bn. For m>n pm == 0.
                    Default: 3.5.  This parameter has no effect if --filter='none' or --filter='gauss'.
                    """,
)
parser.add_argument(
    "-pc",
    default=["0.8"],
    nargs="*",
    help="""
                    List of polynomial coefficients (whitespace seperated) for the PSF FWHM,
                    i.e. -pc p0 p1 ... pn. For m>n pm == 0;
                    Default: 0.8.  This parameter has no effect if --filter='none'.
                    """,
)
parser.add_argument(
    "--lambda0",
    type=float,
    default=7050,
    help="""
                    Zero-point of the polynomials, in Angstrom.  [Default: 7050 Angstrom].
                    This parameter has no effect if --filter='none'.
                    """,
)
parser.add_argument(
    "-t",
    "--threads",
    type=int,
    default=multiprocessing.cpu_count(),
    help=""" Number of CPU cores used in parallel operation.  [Default: all
                    available cpu cores] """,
)
parser.add_argument(
    "--zerothresh",
    default=0.8,
    type=float,
    help="""Numerical artifacts after FFT convolution can be zeroed-out,
i.e. values below this threshold in the final exposure map will be set
to 0.  This is usefull if the exposure map is being used as a weight map. Default: 0.8.""",
)

parser.add_argument(
    "--overwrite", action="store_true", help="Overwrite output files. Use with caution!"
)
parser.add_argument(
    "--pixscale",
    default=0.2,
    type=float,
    help="""Linear extent of a pixel/spaxel in arcseconds. Default: 0.2.
""",
)


args = parser.parse_args()

if args.func == "sum":
    func = np.sum
elif args.func == "mean":
    func = np.mean
elif args.func == "median":
    func = np.median
elif args.func == "max":
    func = np.max
else:
    print("ERROR: --func can only be `sum`, `mean`, `median`, `max`.")
    sys.exit(2)

inputfile = args.input
exp_hdu_num = args.exphdu
outfile = inputfile[: inputfile.find(".fits")] + "_expmap.fits"

program_name = __file__.split("/")[-1]
print(program_name + " from LSDCat version " + str(__version__))
print(
    program_name
    + ": Reading exposure map cube from "
    + inputfile
    + " (HDU "
    + str(exp_hdu_num)
    + ")"
)

hdu = fits.open(inputfile)
exp_cube = hdu[exp_hdu_num].data
exp_cube_header = hdu[exp_hdu_num].header
primary_header = hdu[0].header

command = os.path.basename(sys.argv[0])
for entity in sys.argv[1:]:
    command = command + " " + entity

primary_header["HISTORY"] = (
    "Expmap by LSDCat fov_map_from_expcube.py"
    + " -- "
    + now.strftime("%m/%d/%Y, %H:%M:%S")
)
primary_header["HISTORY"] = "--- start of fov_map_from_expcube.py command ---"
primary_header["HISTORY"] = command
primary_header["HISTORY"] = "--- end of fov_map_from_expcube.py command ---"

exp_cube_header = hierarch_multi_line(
    exp_cube_header, "HIERARCH LSD EFOVIN", inputfile, "expmap input cube"
)
exp_cube_header["HIERARCH LSD EFOVINH"] = (exp_hdu_num, "0-indexed")

xax = exp_cube_header["CRVAL3"] + exp_cube_header["CD3_3"] * (
    np.arange(exp_cube_header["NAXIS3"]) - exp_cube_header["CRPIX3"]
)

if args.filter == "gaussian":
    pc = [float(pcn) for pcn in args.pc][::-1]
    print(
        " Gaussian smoothing of exposure cube. "
        + " FWHM dependence on wavelength modelled as FHWM(λ) = p_i (λ - λ₀)ⁱ with "
        + ", ".join(["p_" + str(i[0]) + "=" + str(i[1]) for i in enumerate(pc[::-1])])
        + " -- λ₀ =  "
        + str(args.lambda0)
        + " Å. "
    )
    filter_windows = spatial_smooth_lib.makeGaussians_poly(
        xax=(xax - args.lambda0),
        p=pc,
        pix_scale=args.pixscale,
        trunc_constant=4.0,
        classic=True,
    )
elif args.filter == "moffat":
    pc = [float(pcn) for pcn in args.pc][::-1]
    bc = [float(bcn) for bcn in args.bc][::-1]
    print(
        " Moffat smoothing of exposure cube. "
        + " FWHM dependence on wavelength modelled as FHWM(λ) = p_i (λ - λ₀)ⁱ with "
        + ", ".join(["p_" + str(i[0]) + "=" + str(i[1]) for i in enumerate(pc[::-1])])
        + " -- λ₀ =  "
        + str(args.lambda0)
        + "Å. "
    )
    print(
        " β dependence as β(λ) = b_i (λ - λ₀)ⁱ with "
        + ", ".join(["b_" + str(i[0]) + "=" + str(i[1]) for i in enumerate(bc[::-1])])
    )
    filter_windows = spatial_smooth_lib.makeMoffats_poly(
        xax=(xax - args.lambda0),
        p=pc,
        b=bc,
        pix_scale=args.pixscale,
        trunc_constant=4,
        classic=True,
    )

elif args.filter == "none":
    pass
else:
    print("ERROR:  --filter must be either 'none', 'gaussian', or 'moffat'. ")
    sys.exit(2)

if args.filter != "none":
    exp_cube = spatial_smooth_lib.filter_parallel(
        filter_windows,
        exp_cube.astype(np.float32),
        args.threads,
        filename=program_name,
        method="fft",
    )


print(
    "fov_map_from_expcube.py - Calculating the "
    + args.func
    + " for all spaxels of the exposure cube..."
)
exp_map = func(exp_cube, axis=0)
exp_map[exp_map < args.zerothresh] = 0.0

print("fov_map_from_expcube.py - ... Done! Saving output " + outfile)
hdu_list = fits.HDUList(
    hdus=[
        fits.PrimaryHDU(header=primary_header, data=None),
        fits.ImageHDU(header=exp_cube_header, data=exp_map),
    ]
)
hdu_list.writeto(outfile, overwrite=args.overwrite)


print("fov_map_from_expcube.py - All done!")
