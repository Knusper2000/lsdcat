#! /usr/bin/env python
# Convert FSF coefficients from MPDAF to LSDCat convention.

# see Appendix B in Herenz 2023

import sys
import pylab as p
from astropy.io import fits
from scipy.special import binom


def mpdaf_pc_to_lsdcat_pc(pc_mpdaf, lambda_0, lambda_1, lambda_2):
    """pc_lsdcat = mpdaf_pc_to_lsdcat_pc(pc_mpdaf, lambda_0, lambda_1, lambda_2)

    Convert polynomial coefficients that describe wavelength dependent
    PSF quantities (FWHM & beta) from MPDAF convention to LSDCat convention.

    MPDAF convention (Eq. 1):
    q(lam) = sum_i^n b_i ((lam - lambda_1)/(lambda_2 - lambda_1) - 0.5)^i

    LSDCat convention (Eq. 2):
    p(lam) = sum_i^n a_i (lam - lambda_0)^i

    Input:
    ------
    pc_mpdaf = list of the coefficents in the MPDAF formula [b_0, b_1, b_2, ..., b_n]
        (Note that in the original MPDAF convention the indices are reversed,
         i.e. b_n = b_1', b_(n-1) = b_2', ... b_0 = b_n'
         but here we use the natural order, i.e. you need to reverse the MPDAF
         output before using this function.

    lambda_0 = the reference wavelength in the LSDCat convention
               (see Eq. 2) - in Angstrom

    lambda_1, lambda_2 = the reference wavelengths in the MPDAF convention
               (see Eq. 1) - in Angstrom

    Output:
    ------
    pc_lsdcat = list of coefficients in the LSDCat formula [a_0, a_1, a_2, ..., a_n]
    """
    n_coeff = len(pc_mpdaf)
    # rescale shorthands
    a = 1 / (lambda_2 - lambda_1)
    b = (-lambda_1) / (lambda_2 - lambda_1) - 0.5
    # now we do the math...
    # inner loop is shift & stretch c_i's ("pure"), the outer loop is
    # shift to a_i's (LSDCat) and the encapsulating loop is over all coefficients
    # - see scanned notes in doc folder
    # ../doc/calc_lsd_cat_poly_notes.jpg
    pc_lsdcat = [
        sum(
            [
                binom(j, i) * lambda_0 ** (j - i) * c_j
                for j, c_j in enumerate(
                    [
                        sum(
                            [
                                binom(j, i) * a**i * b ** (j - i) * b_j
                                for j, b_j in enumerate(pc_mpdaf)
                                if j >= i
                            ]
                        )
                        for i in range(n_coeff)
                    ]
                )
                if j >= i
            ]
        )
        for i in range(n_coeff)
    ]
    return pc_lsdcat


filename = sys.argv[1]
if sys.argv[1] == "-h" or sys.argv[1] == "--help":
    print(
        """
Usage: calc_lsd_cat_poly.py MPDAF_DATACUBE.fits ref_wl (plot)
       MPDAF_DATACUBE.fits is the FITS datacube that contains a header
       with the FSF* keywords
       ref_wl is the LSDCat reference wavelength that is specified with
       --lambda0 in lsd_cc_spatial.py
Output: -pc p0 p1 p2 ... pn -bc b0 b1 b2 .. bn  --lambda0 lambda0
        a string that can be directly copied to the call of lsd_cc_spatial.py
(see also the handwritten notes in ../doc/calc_lsd_cat_poly_notes.jpg
for the formalism that is used)"""
    )
    sys.exit(0)

lambda_0 = float(sys.argv[2])

header = fits.getheader(filename, 0)

# mpdaf coefficients, in reverse, to have a more natural order
fsf_pc_mpdaf = [header["FSF00F" + str(i).zfill(2)] for i in range(header["FSF00FNC"])][
    ::-1
]
fsf_bc_mpdaf = [header["FSF00B" + str(i).zfill(2)] for i in range(header["FSF00BNC"])][
    ::-1
]
lambda_1 = header["FSFLB1"]
lambda_2 = header["FSFLB2"]

fwhm_poly = mpdaf_pc_to_lsdcat_pc(fsf_pc_mpdaf, lambda_0, lambda_1, lambda_2)
beta_poly = mpdaf_pc_to_lsdcat_pc(fsf_bc_mpdaf, lambda_0, lambda_1, lambda_2)

fwhm_poly_string = "-pc " + " ".join([str(f) for f in fwhm_poly])
beta_poly_string = "-bc " + " ".join([str(f) for f in beta_poly])

print(fwhm_poly_string + " " + beta_poly_string + " --lambda0=" + str(lambda_0))
