#!/usr/bin/env python
# FILE: cube-collapse.py
# AUTHOR: C. Herenz (2011-2020)
# DESCR.: Collapse a Datacube
# (Part of LSDCat Suite)

from lsd_cat_lib import get_version

__version__ = get_version()

import sys
import os
import copy
import pylab as p
import warnings

warnings.filterwarnings("ignore", category=RuntimeWarning)

from astropy.io import fits
import argparse

from astropy.utils import exceptions

warnings.filterwarnings("ignore", category=exceptions.AstropyDeprecationWarning)


def conv_to_int(intstr):
    try:
        integer = int(intstr)
        return integer
    except ValueError:
        return intstr


parser = argparse.ArgumentParser(
    description="""
A script to collapse datacubes (i.e. generates a simple mean, variance
 weighted mean, variance weighted mean & signal to noise image from
 datacube). Per default all layers are used for collapsing, but this
 can be changed using the --zstart and --zend switches. May the cat be with you!
""",
    epilog="""
<output> is a FITS file with the following information in the HDUs:
HDU1: variance weighted mean, 
HDU2: variance weighted noise,
HDU3: signal to noise, 
HDU4: simple mean.

""",
)
parser.add_argument(
    "-i",
    "--input",
    required=True,
    type=str,
    help="""
Input FITS datacube, containing at least the signal. The datacube
containing the associated variance could either be in a different HDU
of this file or in another file. 
""",
)
parser.add_argument(
    "-o",
    "--output",
    required=True,
    type=str,
    help="""
Output multi-HDUed FITS file.
""",
)
parser.add_argument(
    "-S",
    "--signalhdu",
    default="DATA",
    help="""
HDU containing the signal (name or 0-indexed number,default: DATA)
""",
)
parser.add_argument(
    "-N",
    "--noisehdu",
    default="STAT",
    help="""
HDU containing the variance (name or 0-indexed number, default: STAT). If no --noisefile is given,
input file will be used.
""",
)
parser.add_argument(
    "-R",
    "--recimahdu",
    default=None,
    help="""
HDU containing a white light image in inputfile. If specified,
this will be used for construction of the outpuf files header.
""",
)
parser.add_argument(
    "--noisefile",
    type=str,
    default=None,
    help="""
If variance is stored in a different file, use this option to specify it.
""",
)
parser.add_argument(
    "--sigma",
    action="store_true",
    help="""
Switch to interpret assumed variance data as standard deviation
instead of variance.
""",
)
parser.add_argument(
    "--zstart",
    default=None,
    type=int,
    help="""
First layer to be used for collapsing (zero-indexed)."
""",
)
parser.add_argument(
    "--zend",
    default=None,
    type=int,
    help="""
Last layer to be used for collapsing (zero-indexed).
""",
)
parser.add_argument(
    "--nantozero",
    action="store_true",
    help="""
Set Nan Values to 0 before operations.""",
)
parser.add_argument(
    "--clobber",
    action="store_true",
    help="""
Overwrite existing file.""",
)

args = parser.parse_args()

# ensuring compatability with old option parser
infilename = args.input
outfilename = args.output
signalhdu = conv_to_int(args.signalhdu)
noisehdu = conv_to_int(args.noisehdu)
if args.recimahdu != None:
    recimahdu = conv_to_int(args.recimahdu)
    WL = True
else:
    WL = False
if args.zstart != None:
    zstart = args.zstart
else:
    zstart = -1
if args.zend != None:
    zend = args.zend
    assert zend > zstart + 1  # do at least collapse 2 layers
else:
    zend = -1

if args.clobber == False and os.path.isfile(outfilename):
    print(
        "File " + outfilename + " exists - use --clobber to force automatic overwrite."
    )
    sys.exit(2)

# actuall collapsing starts here
hdu = fits.open(infilename)
print("Reading Signal... (" + str(infilename) + " HDU:" + str(signalhdu) + ")")
signal = hdu[signalhdu].data  # signal
if args.nantozero:
    signal[p.isnan(signal)] = 0.0

if args.noisefile == None:
    print("Reading Noise...(" + str(infilename) + " HDU:" + str(noisehdu) + ")")
    variance = hdu[noisehdu].data  # variance
else:
    print("Reading Noise...(" + str(args.noisefile) + " HDU:" + str(noisehdu) + ")")
    noise_hdu = fits.open(args.noisefile)
if not args.sigma:
    variance = hdu[noisehdu].data
else:
    # normally noise_hdu[noisehdu] is expected to be
    # variance, unless the --sigma is used
    variance = (noise_hdu[noisedhu].data) ** 2
if args.nantozero:
    variance[p.isnan(variance)] = 0.0

# sub-slicing cube if desired...
if zstart != -1 and zend != -1:
    signal = signal[zstart:zend, :, :]
    variance = variance[zstart:zend, :, :]
elif zstart == -1 and zend != -1:
    signal = signal[:zend, :, :]
    variance = variance[:zend, :, :]
elif zstart != -1 and zend == -1:
    signal = signal[zstart:, :, :]
    variance = variance[zstart:, :, :]

if WL == True:
    recimahead = hdu[recimahdu].header  # wl header
else:
    # if no wl image specified we will reuse the data header:
    recimahead = hdu[signalhdu].header.copy()
    for header_item in [
        "CRPIX3",
        "CD1_3",
        "CD3_1",
        "CD3_2",
        "CD2_3",
        "CD3_3",
        "CUNIT3",
        "CRVAL3",
        "CRPIX3",
        "NAXIS3",
        "BUNIT",
    ]:
        try:
            recimahead.__delitem__(header_item)
        except KeyError:
            pass  # we just ignore key errors :-)

try:
    length = p.shape(signal)[0]
    t2 = p.shape(variance)[0]
    if WL == True:
        t3 = p.shape(hdu[recimahdu].data)[0]
    hdu.close()
except:
    print("ERROR: At least one of the HDUs you want me to work with is empty!")
    sys.exit()

print("Collapsing of the cube starts...(" + str(infilename) + ")")

# simple mean (after division by number of spectral elements...!):
print("Summing up signal in spectral dimension...")

summ = p.sum(signal, 0)
mean = summ / length
print("done!\n")

# variance weighted mean
print("Calculating the variance weigthed mean...(" + str(infilename) + ")")
vmean = p.sum(p.divide(signal, variance), 0) / p.sum(p.divide(1, variance), 0)
print("done!\n")

# variance weighted noise (sigma)
print("Calculating the variance weigthed noise...(" + str(infilename) + ")")

vsigma = p.sqrt(1 / p.sum(1 / variance, 0))
print("done!\n")

# signal to noise
print("Calculating the signal to noise...(" + str(infilename) + ")")
noise = p.sqrt(p.sum(variance, 0) / (length - 1))
mean_noise = noise / p.sqrt(length)  # Eq. 16.207 Taschenbuch
# d. Mathematik, Bronstein et
# al. (6th edition)
SN = mean / mean_noise
print("Calculations for " + str(infilename) + " done!")

# prepare the fits file (attention: python numbering starts with 0, so HDU 1 = HDU 0 in python,
# but gets converted accordingly here)
# HDU 1 -> variance weighted mean
# HDU 2 -> variance weighted noise
# HDU 3 -> Signal to Noise
# HDU 4 -> simple mean
# HDU 5 -> associated noise to the simple mean
meanhead = recimahead.copy()
meanhead["EXTNAME"] = ("VMEAN", "collapsed variance weighted mean")

# meanhead.update('EXTEND',True,after='NAXIS2')
sigmahead = recimahead.copy()
sigmahead["EXTNAME"] = ("SIGMA", "collapsed weighted std. dev.")
snhead = recimahead.copy()
snhead["EXTNAME"] = ("SN", "collapsed S/N")
smeanhead = recimahead.copy()
smeanhead["EXTNAME"] = ("SMEAN", "collapsed simple mean")
noisehead = recimahead.copy()
noisehead["EXTNAME"] = ("SSIGMA", "collapsed std. dev")

print("Writing File " + str(outfilename) + " to disk...")
out = [
    fits.PrimaryHDU(data=vmean, header=meanhead),
    fits.ImageHDU(data=vsigma, header=sigmahead),
    fits.ImageHDU(data=SN, header=snhead),
    fits.ImageHDU(data=mean, header=smeanhead),
    fits.ImageHDU(data=mean_noise, header=noisehead),
]
hdulist = fits.HDUList(out)

# save it
hdulist.writeto(outfilename, output_verify="silentfix", overwrite=True)
print("Done with " + str(infilename) + "!")
print("Output written to " + outfilename)
print(
    """ (HDU1: variance weighted mean, HDU2: variance weighted noise,
 HDU3: signal to noise, HDU4: simple mean, HDU5: associated noise to the simple mean.)"""
)
