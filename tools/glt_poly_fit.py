#! /usr/bin/env python
#
# FILE: glt_poly_fit.py
# AUTHOR: C. Herenz (2013)
# DESCR.: Fits a polynomial of degree 'n' to the ground layer turbulence
#         Modell given in https://musewiki.aip.de/fsf-model
#         and prints the results on the command line

import numpy as np
import os
from astropy.io import fits
from matplotlib import pyplot as plt
import spatial_smooth_lib
import argparse

parser = argparse.ArgumentParser(
    description="""
Fits a polynomial of degree 'n' to the ground layer turbulence
Modell given in https://musewiki.aip.de/fsf-model
and prints the results on the command line."""
)

parser.add_argument(
    "-d",
    "--degree",
    type=int,
    default=2,
    help="""
The degree of the polynomial to fit.""",
)
parser.add_argument(
    "-s",
    "--seeing",
    type=float,
    default=0.8,
    help="""
The DIMM Seeing (measured at 500nm in the zenit)""",
)
parser.add_argument(
    "-l",
    "--scalelength",
    type=float,
    default=22,
    help="""
Outer scale length (median) of the turbulent cells.
""",
)
parser.add_argument(
    "-a",
    "--airmass",
    type=float,
    default=1.0,
    help="""
Airmass of the observation used in the ground-layer turbulence modell.
""",
)
parser.add_argument(
    "-c",
    "--cube",
    type=str,
    default="",
    help="""
Use wavelength grid given in cube. (Default: Use typical MUSE Cube
grid in Angstrom - crval3=4800, crpix3=1., cdelt3=1.25, naxis3 = 3600)""",
)
parser.add_argument(
    "--hdunum",
    type=int,
    default=1,
    help="""
Use HDU Number (default: 1)""",
)
parser.add_argument(
    "--showplot",
    action="store_true",
    help="""
Plot the difference between the polynomial fit and the true model over 
the given wavelength range.""",
)

args = parser.parse_args()

poly_deg = args.degree
dimm = args.seeing
scale_length = args.scalelength
airmass = args.airmass
qsim_datacube = args.cube
hdunum = args.hdunum
cube = os.path.exists(qsim_datacube)
show_plot = args.showplot

if cube == True:
    hdu = fits.open(qsim_datacube)
    header = hdu[hdunum - 1].header
    crval3 = header["CRVAL3"]
    crpix3 = header["CRPIX3"]
    try:
        cdelt3 = header["CD3_3"]
    except KeyError:
        cdelt3 = header["CDELT3"]
    cunit3 = header["CUNIT3"]
    naxis3 = header["NAXIS3"]
else:
    crval3 = 4800.0
    crpix3 = 1.0
    cdelt3 = 1.25
    cunit3 = "Angstrom"
    naxis3 = 3600

# xax is the array containing the wavelength values in nm
if cunit3 == "nm":
    xax = crval3 + cdelt3 * (np.arange(naxis3) - crpix3)
elif cunit3 == "Angstrom":
    xax = 0.1 * crval3 + 0.1 * cdelt3 * (np.arange(naxis3) - crpix3)
elif cunit3 == "um":
    xax = 1e3 * (crval3 + cdelt3 * (np.arange(naxis3) - crpix3))

# FWHMs
fwhms = spatial_smooth_lib.comp_fwhm(xax, dimm, l0=scale_length, am=airmass)
poly_coeff = np.polyfit(xax, fwhms, poly_deg)

print(
    "The FWHM-lambda dependence for the \
ground layer turbulence modell with parameters \n"
)
print(
    "DIMM: " + str(dimm) + " AM: " + str(airmass) + " l_0: " + str(scale_length) + "\n"
)
print("Approximated by a polynomial of degree \n" + str(poly_deg))
print("\nhas the following coefficients (in arcseconds):\n")
for coeff in zip(poly_coeff[::-1], range(len(poly_coeff))):
    print("a_" + str(coeff[1]) + ": %3.8e" % (coeff[0]))

if show_plot == True:
    poly_appx = np.polyval(poly_coeff, xax)
    plt.figure(1, figsize=(8, 4.5))
    plt.subplot(211)
    plt.plot(xax, fwhms, label="GLT model")
    plt.plot(xax, poly_appx, label="Poly n=" + str(poly_deg))
    plt.ylabel("FWHM [asec]")
    plt.legend()
    ax = plt.gca()
    ax.set_xticklabels([])
    plt.minorticks_on()
    plt.subplot(212)
    plt.plot(xax, fwhms - poly_appx, label="Residual model-poly")
    plt.legend()
    plt.minorticks_on()
    plt.xlabel("$\lambda\,\mathrm{[\AA{}]}$ ")
    plt.show()
