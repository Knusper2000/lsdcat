#! /usr/bin/env python3

import argparse, os, sys
from datetime import datetime
from astropy.io import fits

now = datetime.now()

# get string of the commandline that was entered by the user
command = os.path.basename(sys.argv[0])
for entity in sys.argv[1:]:
    command = command + " " + entity

parser = argparse.ArgumentParser(
    description=""" Cut out rectangular
subcube. Rectangle is specified by coordinates of the corners.  WCS information
is preserved."""
)

parser.add_argument("--infile", type=str, help="Input FITS cube", required=True)
parser.add_argument("--xmin", type=int, required=True)
parser.add_argument("--xmax", type=int, required=True)
parser.add_argument("--ymin", type=int, required=True)
parser.add_argument("--ymax", type=int, required=True)
parser.add_argument(
    "--hdus",
    type=str,
    required=True,
    help="""
    Comma separated list of HDU names/numbers to crop.
    """,
)
parser.add_argument("--outfile", type=str, default="")

args = parser.parse_args()

if args.outfile == "":
    outfile = args.infile[:-5] + "_cutted.fits"
else:
    outfile = args.outfile

hdu = fits.open(args.infile)

hdu_names = args.hdus.split(",")
hdu_names = [int(i) for i in hdu_names if i.isdigit()]

primary_hdu = hdu[0]
data_list = [hdu[name].data for name in hdu_names]
header_list = [hdu[name].header for name in hdu_names]
shape_list = [data.shape for data in data_list]

cutted_data_list = [
    (
        data[:, args.ymin : args.ymax, args.xmin : args.xmax]
        if len(shape) == 3
        else data[args.ymin : args.ymax, args.xmin : args.xmax]
    )
    for data, shape in zip(data_list, shape_list)
]
out_header_list = [header.copy() for header in header_list]

for header in out_header_list:
    header["CRPIX1"] -= args.xmin
    header["CRPIX2"] -= args.ymin

primary_hdu.header["HISTORY"] = (
    "Processed by LSDCat cut_subcube.py"
    + " -- "
    + now.strftime("%m/%d/%Y, %H:%M:%S")
)
primary_hdu.header["HISTORY"] = "--- start of cut_subcube.py command ---"
primary_hdu.header["HISTORY"] = command
primary_hdu.header["HISTORY"] = "--- end of cut_subcube.py command ---"

out_hdu_list = fits.HDUList([primary_hdu])
for cutted_data, out_header in zip(cutted_data_list, out_header_list):
    out_hdu_list.append(fits.ImageHDU(cutted_data, out_header))

out_hdu_list.writeto(outfile)
