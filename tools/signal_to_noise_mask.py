#! /usr/bin/env python
# FILE: signal_to_noise_mask.py
# AUTHOR: C. Herenz
# DATE: 2011-03
# INFO: Create a mask from the collapsed signal to noise image,
#       so that a pixels over a certain signal to noise are set
#       to one and and below are set to zero
#       (1st HDU is a cube of the mask - not very memory convinient, but
#       convinient for further masking out in the cube, 2nd HDU is just flat 2D
#       mask, 3rd HDU contains the original mask before evolution (evolution desc. in
#       my Master-Thesis))

import sys
import getopt
import pyfits as fits
import pylab as p
from masklib import *


def usage():
    # print out usage message
    sys.stdout.write(
        """

signal_to_noise_mask.py - Creation of Datacube-Masks from
                          a S/N-cut in the continuum
USAGE:

signal_to_noise_mask.py [-h] -i <Input> -H <HDU> -c <cut> 
			-s <spec_dim> -o <output>

Possible options:
-h [--help]: prints this message
-i [--input]: specify input file (mandatory argument)
-H [--HDU]:  specify the HDU containing the collapsed S/N 
   	     (default: 3).
-o [--output]: specify the filename of the output-file, containing 
   	       the mask-cube (mandatory argument)
-c [--cut]: specify the signal to noise cut (default: 5)
-C [--cycles]: specify the number of evolution cycles (default: 0)
-s [--specdim]: specify the number of spectral elemts of the
   		resulting cube (default: 3463)

"""
    )


def main():
    """ """
    # defaults:
    SN_hdu_num = 2
    SN_cut = 5
    cycles = 0
    spec_dim = 3463  # assuming typical MUSE spectral dimension here
    inSel = False
    outSel = False
    # command-line-parsing
    try:
        options, args = getopt.getopt(
            sys.argv[1:],
            "hi:H:o:c:C:s:",
            ["help", "input=", "HDU=", "output=", "cut=", "cycles=", "specdim="],
        )
    except getopt.error as msg:
        usage()
        sys.exit(2)

    for option, value in options:
        if option in ("-h", "--help"):
            usage()
            sys.exit(0)
        if option in ("-i", "--input"):
            inputfile = str(value)
            if len(inputfile) > 3:
                inSel = True
        if option in ("-H", "--hdu"):
            SN_hdu_num = int(value) - 1
        if option in ("-o", "--output"):
            outputfile = str(value)
            if len(outputfile) > 3:
                outSel = True
        if option in ("-c", "--cut"):
            SN_cut = float(value)
        if option in ("-C", "--cycles"):
            cycles = int(value)
        if option in ("-s", "--specdim"):
            spec_dim = int(value)

    if inSel == False or outSel == False:
        print("Input or Outputfile is a mandatory argument!")
        usage()
        sys.exit(2)

    # MASK CREATION STARTS HERE
    hdu = fits.open(inputfile)
    print(
        "Reading in SN image (HDU"
        + str(SN_hdu_num + 1)
        + " of "
        + str(inputfile)
        + ") ..."
    )
    SN_data = hdu[SN_hdu_num].data
    SN_head = hdu[SN_hdu_num].header
    hdu.close()
    # Create boolean array - False if pixel is above SN
    filtr = SN_data < SN_cut
    oldmask = filtr.copy()
    # evolve the array:
    if cycles > 0:
        evolve(cycles, filtr)
    # now make it as a cube
    print(str(inputfile) + ": Creating the filter cube (this takes some time..)")
    spec_range = p.ones(spec_dim)
    # the next lines stacks all  the 2d arrays to a 3d cube of spectral dimension
    filtr = (
        filtr.transpose()
    )  # <- remember to transpose back if this array needs to be used again
    filter_cube = p.array(
        p.dstack(p.tensordot(spec_range, filtr, axes=0)), dtype=p.int16
    )  # !
    # writing output
    head1 = SN_head.copy()
    head2 = SN_head.copy()
    head3 = SN_head.copy()
    del head1["BUNIT"]
    del head2["BUNIT"]
    del head3["BUNIT"]
    head1.update("EXTNAME", "MASK2D")
    head3.update("EXTNAME", "MASK2D_ORIG")
    filtr = filtr.transpose()
    secondHDU = fits.ImageHDU(data=filtr.astype(p.int16), header=head1)
    thirdHDU = fits.ImageHDU(data=oldmask.astype(p.int16), header=head3)
    # need to transpose here, because of pyfits convention slow to fast
    # here NAXIS3,NAXIS2,NAXIS1 ?
    filter_cube = filter_cube.transpose()
    # preparing Header 2 (MASK 2D-IMAGE):
    head2.update("EXTNAME", "MASK3D")
    head2.update("NAXIS3", 3463, after="NAXIS2")
    head2.update("CRPIX3", 1, comment="Start Wavelength in pixel", after="CRPIX2")
    head2.update("CRVAL3", 480.0, comment="Start Wavelength", after="CRVAL2")
    head2.update("CTYPE3", "AWAV", after="CTYPE2")
    head2.update("CUNIT3", "nm", comment="Wavelength units", after="CUNIT2")
    head2.update("CD1_3", 0)
    head2.update("CD2_3", 0)
    head2.update("CD3_2", 0)
    head2.update("CD3_1", 0)
    head2.update("CD3_3", 1)
    # writing output
    primaryHDU = fits.PrimaryHDU(data=filter_cube.astype(p.int16), header=head2)
    hdulist = fits.HDUList([primaryHDU, secondHDU, thirdHDU])
    hdulist.writeto(
        outputfile, output_verify="silentfix", overwrite=True
    )  # clobber=True could be dangerous here in future
    print(str(inputfile) + ": All done - written output file: " + str(outputfile))


# fire it up!
if __name__ == "__main__":
    main()
