#!/usr/bin/env python
# FILE: lsd_cat_measure.py
# AUTHOR: E. C. Herenz (2023)
# LICENSE: BSD 3-Clause License // https://opensource.org/licenses/BSD-3-Clause
#
# If you make use of this code in your research please cite:
# - Herenz, E. C., & Wisotzki, L. 2017,  A&A 602, A111.
#   https://doi.org/10.1051/0004-6361/201629507
# - Herenz, E. 2023, AN, e606
#   https://doi.org/10.1002/asna.20220091

# lsdcat libraries
import lib.lsd_cat_lib as lsd_cat_lib
import lib.lsd_cat_measure_lib as lsd_cat_measure_lib
from lib.line_em_funcs import (
    get_timestring,
    read_hdu,
    int_or_str,
    string_from_multiline_head,
)
from lib.cats import cats  # the most important import ever!
import spatial_smooth_lib as ssl
import wavelength_smooth_lib as wsl

from astropy.io import fits
from astropy.table import hstack

__version__ = lsd_cat_lib.get_version()

# CONSTANTS THAT ARE RELEVANT FOR THE CATALOG ENTRIES, BUT THEY SHOULD
# BE CHANGED BY THE USER ONLY AT OWN RISK
ws_fixed = 25.0  # half size of the subcubes on which subcubes are
# extracted around the objects for analysis - i.e. 25
# results in 50x50x50 cubes - seems to be enough... if
# a galaxy is bigger than 1/4 of the FOV, than you
# don't wanna measure fluxes with this tool, anyway
z_diff_max = 25  # maximum size of narrow band window ... this value should be OK

times_fwhm_const = 2.5  # multiplicator to define width of the window
# in which weighted positional parameters are determined
muse_spaxscale = 0.2  # '' - probably better to parse from header in
# the future - see
# http://docs.astropy.org/en/stable/api/astropy.wcs.utils.proj_plane_pixel_scales.html#astropy.wcs.utils.proj_plane_pixel_scales

# python standard library
import sys, os, time
import gc
import argparse
import multiprocessing
import string
import random
import math
import re
from datetime import datetime

# required 3rd party libraries (NumPy, SciPy, and Astropy)
import numpy as np
from astropy.io import fits
from astropy.table import Table
from astropy.io.fits import Column

from scipy.ndimage import measurements

# get string of the commandline that was entered by the user
command = os.path.basename(sys.argv[0])
for entity in sys.argv[1:]:
    command = command + " " + entity

# get starting time
starttime = time.time()
now = datetime.now()


# command line parsing
class CustomFormatter(  # argparse.ArgumentDefaultsHelpFormatter,
    argparse.RawDescriptionHelpFormatter
):
    pass


parser = argparse.ArgumentParser(
    description="""
lsd_cat_measure.py - Measurement of emission lines detected with lsd_cat_search.py
""",
    epilog="""
Supported values for --tabvalues option (columns will be appended to input table):

X_SN,Y_SN,Z_SN - 3D S/N weighted centroid coordinates in voxel coordinates 
RA_SN, DEC_SN, LAMBDA_SN - 3D S/N weighted centroid in the physical woorld coordinate systeem 
                           (hereafter physical coordinates).
X_FLUX, Y_FLUX, Z_FLUX - 3D flux weighted centroid in voxel coordinates.
RA_FLUX, DEC_FLUX, LAMBDA_FLUX - 3D flux weighted centroid in physical coordinates.
X_SFLUX, Y_SFLUX, Z_SFLUX - 3D filtered flux weighted centroid in voxel coordinates.
RA_SFLUX, DEC_SFLUX, LAMBDA_SFLUX - 3D filtered flux weighted centroid in physical coordinates.
X_1MOM,Y_1MOM - 1st central moments in flux filtered narrow band (pixel coordinates).
RA_1MOM,DEC_1MOM - 1st central moment in flux filtered narrow band (physical coordinates).
Z_NB_MIN,Z_NB_MAX - Narrowband boundary coordinates (layer coordinates).
LAMBDA_NB_MAX,LAMBDA_NB_MIN - Narrowband boundary coordinates (wavelength - see note below!).

X_2MOM,Y_2MOM,XY_2MOM - Second central moments in flux filtered narrow band.
SIGMA_ISO - Square-root of (X_2MOM + Y_2MOM)/2.
F_KRON,F_2KRON,F_3KRON,F_4KRON - Flux in 1, 2, 3, or 4 Kron-Radii in between Z_NB_MIN and Z_NB_MAX.
F_KRON_ERR,F_2KRON_ERR,F_3KRON_ERR,F_4KRON_ERR - Propagated error on flux measurement.
Z_DIFF_FLAG - Maximum- / Minimum narrow band window used.
RKRON - Kron Radius.
RRON_FLAG - Minimum radius reached in RKRON calculation.

Notes:
1) Z_NB_{MIN,MAX} refers to the layer center coordinates, while
   LAMBDA_NB_MIN and LAMBDA_NB_MAX refer to the layer edges, i.e. λ(Z_NB_{MIN,MAX} ± 0.5).
2) {X,Y,Z,RA,DEC,LAMBDA}_SFLUX, RKRON, RKRON_FLAG, {X,Y,RA,DEC}_1MOM, {X,Y}_2MOM, and
   F_{KRON,2KRON,3KRON,4KRON,KRON_ERR,2KRON_ERR,3KRON_ERR,4KRON_ERR} require a flux-filtered
   cube.  This can be created on the fly, or be supplied with the -ff option (-ffhdu).
   For the latter you can run lsd_cc_{spatial,spectral}.py with the --ignorenoise option.
3) The origin of the voxel based coordinates are following to the choice made in lsd_cat_search.py,
   i.e. per default 1 or, if the --zeroidx switch was used there, 0.
4) Parameters found in the input catalogue always overide command line switches.  
   If you want to do something unorthodox and use different parameters here, you need to delete
   the values in the primary header of your input catalogue.
""",
    formatter_class=CustomFormatter,
)

# dictionary for formatting lines of output catalogue (as in lsd_cat_search.py)
out_line_form = lsd_cat_lib.default_measure_out_line_form


# input catalog
parser.add_argument(
    "-ic",
    "--inputcat",
    required=True,
    type=str,
    help=""" 
                    Input LSDCat catalogue from lsd_cat_search.py (FITS table). This
                    catalogue must contain the fields: I, X_PEAK_SN, Y_PEAK_SN,
                    Z_PEAK_SN. (Required Argument)""",
)
# We try to find the datacube files that where used to create this catalog automatically.
# If this does not work, you can set them manually (see below).
parser.add_argument(
    "-ta",
    "--threshana",
    type=float,
    required=True,
    help="""
                    Analysis threshold.  (Required Argument)
""",
)
parser.add_argument(
    "--tabvalues",
    type=str,
    default="X_SN,Y_SN,Z_SN,RA_SN,DEC_SN,LAMBDA_SN,"
    + "X_FLUX,Y_FLUX,Z_FLUX,RA_FLUX,DEC_FLUX,LAMBDA_FLUX,"
    + "X_SFLUX,Y_SFLUX,Z_SFLUX,RA_SFLUX,DEC_SFLUX,LAMBDA_SFLUX,"
    + "X_1MOM,Y_1MOM,RA_1MOM,DEC_1MOM,Z_NB_MIN,Z_NB_MAX,Z_DIFF_FLAG,"
    + "LAMBDA_NB_MIN,LAMBDA_NB_MAX,X_2MOM,Y_2MOM,XY_2MOM,"
    + "RKRON,RKRON_FLAG,SIGMA_ISO,F_KRON,F_KRON_ERR,"
    + "F_2KRON,F_2KRON_ERR,F_3KRON,F_3KRON_ERR,F_4KRON,F_4KRON_ERR",
    help=""" 
                    Comma-separated list of values to be written in output
                    catalog. (Default: all).  """,
)
parser.add_argument(
    "-f",
    "--fluxcube",
    required=False,
    default=None,
    type=str,
    help=""" 
                    FITS file containing the flux (+ variance) datacubes on which
                    flux measurments are performed.  If not specified we try to use
                    the cube that was used as input for the matched filter processing
                    in lsd_cc_{spectral,spatial}.py prior to lsd_cat_search.py, which
                    HIERARCH LSD CCSIN keyword in the primary header of the input
                    LSDCat catalogue.""",
)
parser.add_argument(
    "--fhdu",
    default="MFGSUBDATA",
    required=False,
    type=int_or_str,
    help="""
                    HDU (name or 0-indexed number) in fluxcube FITS file to use for
                    measurements.  Per default we try to use the HDU name or
                    0-indexed number specified in `HIERARCH LSD CCSINS` keyword in
                    the primary header of the input LSDCat catalogue.  If (and only
                    if) this fails we assume `MFGSUBDATA` as the default, but this
                    can be changed here.""",
)
parser.add_argument(
    "--ferrhdu",
    default="none",
    required=False,
    type=int_or_str,
    help=""" 
                    HDU (name or 0-indexed number) in fluxcube FITS that stores the
                    variances needed to determine the statistical error on the flux
                    measurements.  Per default we try to use the `HIERRCH LSD CCSINN`
                    keyword in the primary header of the input LSDCat catalogue.""",
)
parser.add_argument(
    "-ff",
    "--filteredfluxcube",
    default="none",
    required=False,
    type=str,
    help="""
                    FITS file containing the flux filtered (smoothed) datacube.  Some
                    measurments (see above) require such a smoothed datacube, as they
                    are not so robust for faint emission lines in the presence of
                    noise.  If not specified, we create this on the fly from the
                    fluxcube's fhdu using as smoothing kernel the spatial- and
                    spectral profile that was used to construct the matched filter
                    template.  To this aim we try to find the template parameters
                    (`HIERARCH LSD CCS PLYPP#` [where # is a number], `HIERARCH LSD
                    CCS PLYPL0`, and `HIERARCH CCLVFWHM`) from the primary header of
                    the input LSDCat catalogue.  In this case the performed operation
                    is exactly the same as if lsd_cc_{spatial,spectral}.py where run
                    with the --ignorenoise option.""",
)
parser.add_argument(
    "--ffhdu",
    default="DATA_3DCC",
    type=int_or_str,
    help="""
                    HDU name (or number) in --filteredfluxcube that
                    contains the filtered flux data. [Default: DATA_3DCC].
                    Used if -ff/--filteredfluxcube was specified, no effect otherwise.
""",
)
parser.add_argument(
    "-sn",
    "--sncube",
    type=str,
    required=False,
    default="none",
    help="""
                    FITS file containing the matched-filtered datacube.  Per default
                    we try to use the FITS file that was used as input for
                    lsd_cat_search.py from the `HIERARCH LSD CATIN` keyword the
                    primary header of the input LSDCat catalogue.
""",
)
parser.add_argument(
    "--ffsnhdu",
    default="none",
    type=int_or_str,
    help=""" 
                    HDU (name or 0-indexed number) that contains the matched-filtered
                    datacube in the FITS file.  Per default we try to use the HDU name
                    specfied in the `HIERARCH LSD SHDU` keyword in the primary header
                    of the input LSDCat catalogue.""",
)
parser.add_argument(
    "-t",
    "--thresh",
    type=float,
    default=None,
    required=False,
    help="""
                    The detection threshold that was used in the lsd_cat_search.py
                    run.  Per default we try to use the value specified in the
                    `HIERARCH LSD THRESH` keyword in the primary header of the input
                    LSDCat catalogue.  """,
)

parser.add_argument(
    "--rmin",
    type=float,
    default=3.0,
    help="""
                    Minimum radius for flux extraction aperture (in units of spectral
                    pixels). [default: 3] """,
)
parser.add_argument(
    "--rmax",
    type=float,
    default=6.0,
    help="""
                    Maximum radius for flux extraction aperture (in units of
                    circularised square root of the variance of the flux
                    distribution, known in SExtractor as isophotal radius; output
                    parameter SIGMA_ISO).  This radius also defines the area that is
                    used to calculate the Kron-radius. [default: 6] """,
)
parser.add_argument(
    "-pc",
    default=None,
    nargs="*",
    required=False,
    help=""" 
                    List of polynomial coefficients (whitespace seperated) used for
                    the PSF FWHM template, i.e. -pc p0 p1 ... pn and for m>n pm == 0
                    (same syntax as in lsd_cc_spatial.py). Per default we try to read
                    those from the `HIERARCH LSD CCSPLYP#` (where # is an integer)
                    keywords in the primary header of the input LSDCat catalogue.
                    """,
)
parser.add_argument(
    "-bc",
    default=None,
    nargs="*",
    required=False,
    help="""
                    List of polynomial coefficients (whitespace seperated) for the
                    beta parameter of the Moffat profile, i.e. -bc b0 b1 b2 .. bn and
                    for m>n bm == 0.  Per default we try to read those from the
                    `HIERARCH LSD CCSB#` (where # is an integer) keywords in the
                    primary header of the input LSDCat catalogue, if the `HIERARCH
                    LSD CCSBETA` keyword is present.  Otherwise a Gaussian spatial
                    template is used. """,
)
parser.add_argument(
    "-lambda0",
    default=None,
    nargs="*",
    required=False,
    help="""
                    Zero-point of the polynomials, in Angstrom.  Per default we try
                    to read this from the `HIERARCH LSD CCSPLYL0` keyword in the
                    primary header of the input LSDCat catalogue.""",
)
parser.add_argument(
    "-F",
    "--FWHM",
    type=float,
    default=None,
    required=False,
    help="""
                    Specify the FWHM of the Gaussian line template in km/s. Per
                    default we try to read this from the `HIERARCH LSD CCLVFWHM`
                    keyword in the primary header of the input LSDCat catalogue.
                    """,
)
parser.add_argument(
    "-c",
    "--catalog",
    type=str,
    default=None,
    required=False,
    help="""
                    Filename of the FITS table containing the
                    output catalogue with the columns as defined in
                    TABVALUES. [Default: INPUTCAT+`_measured.fits`]
                    """,
)
parser.add_argument(
    "--overwrite",
    action="store_true",
    required=False,
    help="""
                    Overwrite already existing output file! 
                    USE WITH CAUTION AS THIS MAY OVERWRITE YOUR RESULTS!
                    """,
)
parser.add_argument(
    "--threads",
    type=int,
    default=multiprocessing.cpu_count(),
    help="""
                    Number of CPU cores used in parallel operation. 
                    [Default: all available cpu cores]
                    """,
)

args = parser.parse_args()

table_filename = args.inputcat

R_min = args.rmin
R_max = args.rmax

if args.catalog == None:
    assert table_filename[-5:] == ".fits"
    outfitstable_name = table_filename[:-5] + "_measured.fits"
else:
    outfitstable_name = args.catalog
    assert outfitstable_name[-5:] == ".fits"

if args.overwrite != True and os.path.isfile(outfitstable_name):
    print(
        outfitstable_name
        + " already exists. Use --overwrite to overwrite! ABORTING NOW!"
    )
    sys.exit(2)

tabvalues = args.tabvalues.split(",")
# test if all entries in --tabvalues are recognised
lsd_cat_lib.tabvalue_test(tabvalues, out_line_form=out_line_form)

print(" ... LSDCat Measurement Routine Version " + str(__version__) + " ... ")
print(cats[3])

# open input catalogue
table = Table.read(table_filename, format="fits")
table_header = fits.getheader(table_filename, 0)  # primary header

try:
    origin = int(table_header["LSD ORIGIN"])
except KeyError:
    print(
        """WARNING: Key `HIERARCH LSD ORIGIN` not found. Assuming origin of the
    coordinates in input table are 0."""
    )
    origin = 0

# minimum cataloge information required to make measurements
try:
    running_ids = table["I"]
    x_peak_sns = table["X_PEAK_SN"].astype(int) - origin
    y_peak_sns = table["Y_PEAK_SN"].astype(int) - origin
    z_peak_sns = table["Z_PEAK_SN"].astype(int) - origin
except KeyError:
    print(
        "ERROR: provided input catalog "
        + table_filename
        + " does not contain the required minimum input fields: "
        + "I, X_PEAK_SN, Y_PEAK_SN, Z_PEAK_SN"
    )
    sys.exit(2)

# detection threshold
try:
    thresh = float(table_header["LSD THRESH"])
    print("Detection threshold `LSD THRESH` from input header: " + str(thresh))
except KeyError:
    if args.thresh == None:
        print(
            """ERROR: NO DETECTION THRESHOLD PROVIDED (--thresh) AND NONE FOUND IN HEADER
        OF INPUT CATALOGUE!"""
        )
        sys.exit(2)
    else:
        thresh = args.thresh
        print("Detection Threshold set via --thresh: " + str(thresh))


# analysis threshold (required parameter)
thresh_ana = args.threshana
table_header["LSD THRESHANA"] = (thresh_ana, "analysis threshold")
if thresh_ana > thresh:
    print(
        """ERROR: ANALYSIS THRESHOLD > DETECTION THRESHOLD. (%2.3f > %2.3f)! 
          """
        % (thresh_ana, thresh)
    )
    sys.exit(2)

# read in template parameters
# 1) spatial
try:
    ccs_method = table_header["LSD CCSFILT"]
    pc = [
        table_header[key]
        for key in table_header.keys()
        if bool(re.search("\d$", key)) and key.startswith("LSD CCSPLYP")
    ][::-1]
    if ccs_method == "Moffat":
        use_gaussian = False
        bc = [
            table_header[key]
            for key in table_header.keys()
            if bool(re.search("\d$", key)) and key.startswith("LSD CCSB")
        ][::-1]
    elif ccs_method == "Gaussian":
        use_gaussian = True
except KeyError:
    if args.pc == None:
        print(
            "ERROR: -pc not set and LSD CCS  keywords in the header of input catalogue."
        )
        sys.exit(2)
    elif args.pc != None and args.bc == None:
        pc = args.pc[::-1]
        print(
            "Gaussian FHWM(lambda) dependence via polynomial with coefficients (set via -pc) "
            + ", ".join(
                ["p" + str(i[0]) + "=" + str(i[1]) for i in enumerate(pc[::-1])]
            )
        )
        use_gaussian = True
    elif args.pc != None and args.bc != None:
        pc = args.pc[::-1]
        bc = args.bc[::-1]
        print(
            "Moffat FHWM(lambda) dependence via polynomial with coefficients (set via -pc) "
            + ", ".join(
                ["p" + str(i[0]) + "=" + str(i[1]) for i in enumerate(pc[::-1])]
            )
        )
        print(
            "Moffat beta(lambda) dependence via polynomial with coefficients (set via -bc "
            + ", ".join(
                ["b" + str(i[0]) + "=" + str(i[1]) for i in enumerate(bc[::-1])]
            )
        )
        use_gaussian = False
    else:
        print("Detected an ERROR in front of the computer.")
        print(cats[1])
        sys.exit(2)

try:
    lambda0 = float(table_header["LSD CCSPLYL0"])
    print(
        "lambda0 for evaluation of polynomials from `HIERARCH LSD CCSPL0`: "
        + str(lambda0)
    )
except KeyError:
    if args.lambda0 == None:
        print(
            "ERROR: `HIERARCH LSD CCSPLYL0 not in header of input catalogue "
            + "and --lambda0 not set."
        )
        sys.exit(2)
    else:
        lambda0 = args.lambda0
        print("lambda0 for evaluation of polynomials from --lambda0: " + str(lambda0))

# 2) spectral
try:
    ccl_vfwhm = table_header["LSD CCLVFWHM"]
    print(
        "FWHM of the Gaussian line template in km/s from `HIERARCH LSD CCLVFWHM`: "
        + str(ccl_vfwhm)
    )
except KeyError:
    if args.FWHM == None:
        print(
            """ ERROR: `HIERARCH LSD CCLVFWHM` not found in header of input and --FWHM parameter
            not used. """
        )
        sys.exit(2)
    else:
        ccl_vfwhm = args.FWHM
        print(
            "FWHM of the Gaussian line template in km/s from --FWHM: " + str(ccl_vfwhm)
        )

#  SN Cube
try:
    sncube_file = string_from_multiline_head(table_header, "LSD CATIN")
    assert os.path.isfile(
        sncube_file
    ), f"{sncube} for `HIERACH LSD CATIN` not a valid file."
    print(sncube_file + "from `HIERARCH LSD CATIN` of " + table_filename)
    sncube_hdu = table_header["LSD SHDU"]
    print("SN HDU: " + str(sncube_hdu) + "from `HIERARCH LSD SHDU`")
except KeyError:
    if args.sncube == "none":
        print("""ERROR: No input matched filter cube provided, and --sncube not set.""")
        sys.exit(2)
    else:
        sncube_file = args.sncube
        sncube_hdu = args.ffsnhdu
        assert os.path.isfile(sncube_file), f"{sncube} from --sncube is not a file."
        print(sncube_file + " from --sncube")
        print("SN HDU: " + str(sncube_hdu) + " from --ffsnhdu")

print(
    table_filename
    + ": Opening matched-filtered cube "
    + sncube_file
    + " (HDU "
    + str(sncube_hdu)
    + ") ... "
    + get_timestring(starttime)
)
sn_cube, sn_cube_header = read_hdu(
    sncube_file, sncube_hdu, nans_to_value=True, nanvalue=0.0
)


# values that require flux cube and filtered flux cube
ff_values_list = [
    "F_KRON",
    "F_2KRON",
    "F_3KRON",
    "F_4KRON",
    "F_KRON_ERR",
    "F_2KRON_ERR",
    "F_3KRON_ERR",
    "F_4KRON_ERR",
    "RKRON" "SIGMA_ISO",
    "X_1MOM",
    "Y_1MOM",
    "RA_1MOM",
    "DEC_1MOM",
    "X_2MOM",
    "Y_2MOM",
    "RKRON_FLAG",
    "X_SFLUX",
    "Y_SFLUX",
    "Z_SFLUX",
    "RA_SFLUX",
    "DEC_SFLUX",
    "LAMBDA_SFLUX",
]
if any([True for elem in ff_values_list if elem in tabvalues]):
    try:
        fluxcube_file = string_from_multiline_head(table_header, "LSD CCSIN")
        fluxcube_hdu = table_header["LSD CCSINS"]
        fluxcube_err_hdu = table_header["LSD CCSINN"]
        assert os.path.isfile(
            fluxcube_file
        ), f"{fluxcube_file} for `HIERACH LSD CCSIN` not a valid file."
        print(
            "Fluxcube "
            + fluxcube_file
            + " from `HIERACH LSD CCSIN` in "
            + table_filename
        )
        print("Fluxcube HDU: " + str(fluxcube_hdu) + " from `HIERARCH LSD CCSINS`")
        print(
            "Fluxcube Variance HDU: "
            + str(fluxcube_err_hdu)
            + " from `HIERARCH LSD CCSINN`."
        )
    except KeyError:
        if args.fluxcube == None:
            print(
                "ERROR: No `HIERARCH LSD CCSIN` or `HIERARCH LSD CCSINS` "
                + "keyword and --fluxcube not set."
            )
            sys.exit(2)
        else:
            fluxcube_file = args.fluxcube
            fluxcube_hdu = args.fhdu
            fluxcube_err_hdu = args.ferrhdu
            assert os.path.isfile(
                fluxcube_file
            ), f"{fluxcube_file} from --fluxcube not a valid file."
            print("Fluxcube " + fluxcube_file + " from --fluxcube.")
            print(
                "Fluxcube HDU: "
                + str(fluxcube_hdu)
                + " from --fhdu (or the default there)."
            )

    fluxcube_data, fluxcube_header = read_hdu(
        fluxcube_file, fluxcube_hdu, nans_to_value=True, nanvalue=0
    )

    assert fluxcube_header["CUNIT3"] == "Angstrom"
    cunit3 = fluxcube_header["CUNIT3"]
    crval3 = fluxcube_header["CRVAL3"]
    cdelt3 = fluxcube_header["CD3_3"]
    crpix3 = fluxcube_header["CRPIX3"]
    naxis3 = fluxcube_header["NAXIS3"]

    # wavelength axis
    xax = crval3 + cdelt3 * ((np.arange(naxis3) + 1) - crpix3)

    if args.filteredfluxcube == "none":
        print(" ... Generating smoothed flux cube for measurments")
        # spatial smoothing
        if use_gaussian:
            filter_windows = ssl.makeGaussians_poly(
                xax=(xax - lambda0), p=pc, pix_scale=muse_spaxscale, classic=True
            )
        else:
            filter_windows = ssl.makeMoffats_poly(
                xax=(xax - lambda0), p=pc, b=bc, pix_scale=muse_spaxscale, classic=True
            )
        print(
            "Average size of the filter windows  "
            + str(filter_windows[int(len(filter_windows) / 2)].shape[0])
        )

        s_smoothed = ssl.filter_parallel(
            filter_windows,
            fluxcube_data,
            args.threads,
            filename="Spatial smoothing",
            method="fft",
        )

        # wavelength smoothing
        filter_matrix = wsl.create_filter_matrix_vel(
            ccl_vfwhm, lambda_start=xax[0], cdelt=cdelt3, lMax=fluxcube_data.shape[0]
        )
        flux_filt_cube = wsl.filter_parallel(
            filter_matrix,
            fluxcube_data.reshape(
                fluxcube_data.shape[0], fluxcube_data.shape[1] * fluxcube_data.shape[2]
            ),
            args.threads,
            filename="Wavelength smoothing",
        )
        flux_filt_cube = flux_filt_cube.reshape(
            filter_matrix.shape[0], fluxcube_data.shape[1], fluxcube_data.shape[2]
        )
        start = int((filter_matrix.shape[0] - filter_matrix.shape[1]) / 2)
        end = int(flux_filt_cube.shape[0] - start)
        flux_filt_cube = flux_filt_cube[start:end, :, :]

    else:
        print(
            "... Reading in filtered flux cube from file"
            + args.filteredfluxcube
            + " (HDU "
            + str(args.ffhdu)
            + "). "
            + get_timestring(starttime)
        )
        flux_filt_cube, flux_filt_header = read_hdu(
            args.filteredfluxcube, args.ffhdu, nans_to_value=True, nanvalue=0.0
        )


### ACTUAL MEASUREMENTS START HERE ###

# - subcube boundaries around each detection from filter parameters
x_min, x_max, y_min, y_max, z_min, z_max = lsd_cat_measure_lib.calc_borders_from_filter(
    x_peak_sns,
    y_peak_sns,
    z_peak_sns,
    sn_cube,
    xax,
    cunit3,
    pc,
    lambda0,
    ccl_vfwhm,
    times_fwhm=times_fwhm_const,
    spaxel_scale=muse_spaxscale,
)


# S/N weighted barycenter
sn_weighted_list = ["X_SN", "Y_SN", "Z_SN", "LAMBDA_SN", "RA_SN", "DEC_SN"]
if any([True for element in sn_weighted_list if element in tabvalues]):
    print(
        table_filename
        + ": Calculating (SN weigthed) barycenter of "
        + "detections ... "
        + get_timestring(starttime)
    )
    x_sn_com, y_sn_com, z_sn_com = lsd_cat_lib.calc_weighted(
        x_min, x_max, y_min, y_max, z_min, z_max, sn_cube, thresh_ana=thresh_ana
    )
    lambda_sn = lsd_cat_lib.calc_lambda(z_sn_com, fluxcube_header)

    ra_sn_com, dec_sn_com = lsd_cat_lib.pix_to_radec(
        x_sn_com, y_sn_com, fluxcube_header
    )

# filtered flux weighted barycenter
fs_weighted_list = [
    "X_SFLUX",
    "Y_SFLUX",
    "Z_SFLUX",
    "RA_SFLUX",
    "DEC_SFLUX",
    "LAMBDA_SFLUX",
]
if any([True for element in fs_weighted_list if element in tabvalues]):
    print(
        table_filename
        + ": Calculating (filtered flux weigthed) barycenter "
        + "of detections ... "
        + get_timestring(starttime)
    )
    x_sflux_com, y_sflux_com, z_sflux_com = lsd_cat_lib.calc_weighted(
        x_min,
        x_max,
        y_min,
        y_max,
        z_min,
        z_max,
        sn_cube,
        thresh_ana=thresh_ana,
        weigh_cube=flux_filt_cube,
    )
    lambda_sflux_com = lsd_cat_lib.calc_lambda(z_sflux_com, fluxcube_header)
    ra_sflux_com, dec_sflux_com = lsd_cat_lib.pix_to_radec(
        x_sflux_com, y_sflux_com, fluxcube_header
    )


# z_min, z_max above analysis threshold - defines NB window for flux extraction
print(
    table_filename
    + ": Calculating NB windows for moment based "
    + "parametrisation of sources ... "
    + get_timestring(starttime)
)
z_mins, z_maxs = lsd_cat_measure_lib.nb_zmin_zmax(
    sn_cube, x_peak_sns, y_peak_sns, z_peak_sns, thresh_ana, ws=ws_fixed
)

# if the window gets to broad we fix it to (20 per default, in MUSE
# thats >20A) spectral layers max
z_mins, z_maxs, z_diff_flag = lsd_cat_measure_lib.z_diff_calc(
    z_mins, z_maxs, z_peak_sns, z_diff_max=z_diff_max
)

# NB window in wavelength coordinates, but measuring the border of the wavelength bin,
# not the center as in pixel coordinates
lambda_mins = lsd_cat_lib.calc_lambda(z_mins, fluxcube_header) - cdelt3 / 2.0
lambda_maxs = lsd_cat_lib.calc_lambda(z_maxs, fluxcube_header) + cdelt3 / 2.0

# logical masks at peak layers defining the isophotes for calculation
# of sigma_isos
print(
    table_filename
    + ": Calculating isophotoes at analysis threshold ..."
    + get_timestring(starttime)
)
logical_masks_at_peak = lsd_cat_measure_lib.ana_thresh_region_at_peak(
    sn_cube, x_peak_sns, y_peak_sns, z_peak_sns, thresh_ana
)

# summation from z_min -> z_max + 1 (to include last layer) over filtered MFS cube
print(
    table_filename
    + ": Creating NB images of filtered fluxcube ..."
    + get_timestring(starttime)
)
nb_images = lsd_cat_measure_lib.gen_nb_images(flux_filt_cube, z_mins, z_maxs + 1)

# select regions relevant for image moments
print(
    table_filename
    + ": Select source pixels that are relevant"
    + " for image moments ..."
    + get_timestring(starttime)
)
masked_flux_filt_nb_images = lsd_cat_measure_lib.gen_masked_flux_filt_nb_images(
    logical_masks_at_peak, nb_images
)

# image moments from those regions
print(
    table_filename
    + ": Calculation of image moments for the sources ..."
    + get_timestring(starttime)
)
x_1moms, y_1moms, x_2moms, y_2moms, xy_2moms = lsd_cat_measure_lib.sn_2dmoms(
    masked_flux_filt_nb_images
)
# Conversion of x_1moms & y_1moms to right ascension and declination
ra_1moms, dec_1moms = lsd_cat_lib.pix_to_radec(x_1moms, y_1moms, fluxcube_header)

print(
    table_filename
    + ": Calculation of parameters derived from image moments "
    + "(e.g. ellipse parameters) ..."
    + get_timestring(starttime)
)

# sigma_iso_circ = std. dev assuming circular symmetric Gaussian dist
sigma_isos = lsd_cat_lib.sigma_iso_circ(x_2moms, y_2moms)


# Create the arrays from which the kron radius will be caluclated.
# These arrays are created by selecting a region R_max * sigma_iso
# centered on x_1mom, x_2mom from each masked_flux_filt_nb_image
print(
    table_filename
    + ": Select pixels that are relevant"
    + " for Kron radius calculation ..."
    + get_timestring(starttime)
)
kron_calc_images = lsd_cat_measure_lib.create_kron_calc_images(
    masked_flux_filt_nb_images, x_1moms, y_1moms, sigma_isos, R_max
)

# calculation of Kron radii
print(table_filename + ": Calculation of Kron radii ..." + get_timestring(starttime))
r_krons, r_kron_flag = lsd_cat_measure_lib.calc_rkrons(
    x_1moms, y_1moms, kron_calc_images, R_min=R_min
)


# open flux cube if flux values are demanded
flux_values_list = [
    "F_KRON",
    "F_2KRON",
    "F_3KRON",
    "X_FLUX",
    "Y_FLUX",
    "Z_FLUX",
    "RA_FLUX",
    "DEC_FLUX",
    "LAMBDA_FLUX",
]
# flux_err_values_list = ['F_KRON_ERR','F_2KRON_ERR','F_3KRON_ERR']
flux_values_demanded = any(
    [True for element in flux_values_list if element in tabvalues]
)
# flux_err_values_demanded = any([True for element in flux_err_values_list
#                             if element in tabvalues])
if flux_values_demanded and "fluxcube_data" not in globals():
    print(
        table_filename
        + ": Opening flux-cube "
        + fluxcube_file
        + "(FluxHDU: "
        + str(fluxcube_hdu)
        + ") "
        + get_timestring(starttime)
    )
    del flux_filt_cube  # freeing some memory
    # del sn_cube
    mfs_cube, mfs_cube_header = read_hdu(
        fluxcube_file, fluxcube_hdu, nans_to_value=True, nanvalue=0
    )
    print(
        table_filename
        + ": Flux variances loading from "
        + fluxcube_file
        + "(FluxErrHDU: "
        + str(fluxcube_err_hdu)
        + ")"
    )
    mfs_cube_err, mfs_cube_err_header = read_hdu(
        fluxcube_file, fluxcube_err_hdu, nans_to_value=True, nanvalue=0
    )
elif flux_values_demanded and "fluxcube_data" in globals():
    del flux_filt_cube
    mfs_cube = fluxcube_data
    mfs_cube_header = fluxcube_header
    print(
        table_filename
        + ": Flux variances loading from "
        + fluxcube_file
        + " (FluxErrHDU: "
        + str(fluxcube_err_hdu)
        + ")"
    )
    mfs_cube_err, mfs_cube_err_header = read_hdu(
        fluxcube_file, fluxcube_err_hdu, nans_to_value=True, nanvalue=0
    )

    # flux weighted barycenter
flux_weighted_list = [
    "X_FLUX",
    "Y_FLUX",
    "Z_FLUX",
    "RA_FLUX",
    "DEC_FLUX",
    "LAMBDA_FLUX",
]
if any([True for element in flux_weighted_list if element in tabvalues]):
    print(
        table_filename
        + ": Calculating (flux weigthed) barycenter "
        + "of detections ... "
        + get_timestring(starttime)
    )
    x_flux_com, y_flux_com, z_flux_com = lsd_cat_lib.calc_weighted(
        x_min,
        x_max,
        y_min,
        y_max,
        z_min,
        z_max,
        sn_cube,
        thresh_ana=thresh_ana,
        weigh_cube=mfs_cube,
    )
    lambda_flux_com = lsd_cat_lib.calc_lambda(z_flux_com, mfs_cube_header)
    ra_flux_com, dec_flux_com = lsd_cat_lib.pix_to_radec(
        x_flux_com, y_flux_com, mfs_cube_header
    )


if any([True for element in ["F_KRON", "F_KRON_ERR"] if element in tabvalues]):
    print(table_filename + ": Flux + error measurment in Kron appertures... ")
    print(table_filename + ": k_scale = 1 ... " + get_timestring(starttime))

    f_krons, f_krons_err = lsd_cat_measure_lib.fluxes_rkron_circ(
        mfs_cube,
        x_1moms,
        y_1moms,
        z_mins,
        z_maxs,
        r_krons,
        k_scale=1.0,
        err_cube=mfs_cube_err,
        delta_lambda=cdelt3,
    )

if any([True for element in ["F_2KRON", "F_2KRON_ERR"] if element in tabvalues]):
    print(table_filename + ": k_scale = 2 ... " + get_timestring(starttime))

    f_2krons, f_2krons_err = lsd_cat_measure_lib.fluxes_rkron_circ(
        mfs_cube,
        x_1moms,
        y_1moms,
        z_mins,
        z_maxs,
        r_krons,
        k_scale=2.0,
        err_cube=mfs_cube_err,
        delta_lambda=cdelt3,
    )

if any([True for element in ["F_3KRON", "F_3KRON_ERR"] if element in tabvalues]):
    print(table_filename + ": k_scale = 3 ..." + get_timestring(starttime))

    f_3krons, f_3krons_err = lsd_cat_measure_lib.fluxes_rkron_circ(
        mfs_cube,
        x_1moms,
        y_1moms,
        z_mins,
        z_maxs,
        r_krons,
        k_scale=3.0,
        err_cube=mfs_cube_err,
        delta_lambda=cdelt3,
    )

if any([True for element in ["F_4KRON", "F_4KRON_ERR"] if element in tabvalues]):
    print(table_filename + ": k_scale = 4 ..." + get_timestring(starttime))

    f_4krons, f_4krons_err = lsd_cat_measure_lib.fluxes_rkron_circ(
        mfs_cube,
        x_1moms,
        y_1moms,
        z_mins,
        z_maxs,
        r_krons,
        k_scale=4.0,
        err_cube=mfs_cube_err,
        delta_lambda=cdelt3,
    )


# writing output fits table
line_string, var_list, unit_list, fits_format_list = lsd_cat_lib.gen_line_string(
    tabvalues,
    vars(),
    out_line_form=out_line_form,
    index_sort=None,
    coordinate_list=[
        "X_SN",
        "Y_SN",
        "Z_SN",
        "X_FLUX",
        "Y_FLUX",
        "Z_FLUX",
        "Z_NB_MIN",
        "Z_NB_MAX",
        "X_SFLUX",
        "Z_SFLUX",
        "Z_SFLUX",
        "X_1MOM",
        "Y_1MOM",
    ],
    zeroidx=not bool(origin),
)


new_tbhdu = lsd_cat_lib.make_fits_cat(
    table_header, tabvalues, var_list, unit_list, fits_format_list
)

# this is a bit clumsy - but we need to have Table objects for
# hstack to work
tempfile_name = lsd_cat_lib.gen_tempfilename()
new_tbhdu.writeto(tempfile_name)
new_tab = Table.read(tempfile_name)
out_table = hstack([table, new_tab])
out_table_hdu = fits.table_to_hdu(out_table)

table_header["HISTORY"] = (
    "Created by LSDCat lsd_cat_measure.py "
    + " -- "
    + now.strftime("%m/%d/%Y, %H:%M:%S")
)
table_header["HISTORY"] = "--- start of lsd_cat_measure.py command ---"
table_header["HISTORY"] = command
table_header["HISTORY"] = "--- end of lsd_cat_measure.py command ---"

primary_hdu = fits.PrimaryHDU(header=table_header)
out_hdu_list = fits.HDUList([primary_hdu, out_table_hdu])

out_hdu_list.writeto(outfitstable_name, overwrite=args.overwrite)
os.remove(tempfile_name)


print(
    table_filename
    + ": DONE!!! Wrote FITS catalog "
    + outfitstable_name
    + " "
    + get_timestring(starttime)
)


print(cats[4])  # ;-)
