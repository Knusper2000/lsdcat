#!/usr/bin/env python
## FILE: lsd_cat_search.py
# AUTHOR: Edmund Christian Herenz
# LICENSE: BSD 3-Clause License // https://opensource.org/licenses/BSD-3-Clause
#
# If you make use of this code in your research please cite:
# - Herenz, E. C., & Wisotzki, L. 2017,  A&A 602, A111.
#   https://doi.org/10.1051/0004-6361/201629507
# - Herenz, E. 2023, AN, e606
#   https://doi.org/10.1002/asna.20220091


# lsdcat libraries
import lib.lsd_cat_lib as lsd_cat_lib
from lib.line_em_funcs import get_timestring, read_hdu, int_or_str, search_header
from lib.cats import cats

__version__ = lsd_cat_lib.get_version()

# python standard library
from datetime import datetime
import sys, os, time
import gc
import argparse
import string
import random
import math

# science / astronomy
import numpy as np
from astropy.io import fits
from astropy.io.fits import Column
from astropy.wcs import WCS

# scipy > 1.8 deprecates measurement namespace (ugly hack)
import scipy

sp_ver = scipy.__version__.split(".")
if int(sp_ver[0]) == 1 and int(sp_ver[1]) < 8:
    from scipy.ndimage import measurements
elif (int(sp_ver[0]) == 1 and int(sp_ver[1]) >= 8) or (int(sp_ver[0]) > 1):
    import scipy.ndimage as measurements
else:
    print("ABORT:  Scipy Version > 1.0 required!")

# get string of the commandline that was entered by the user
command = os.path.basename(sys.argv[0])
for entity in sys.argv[1:]:
    command = command + " " + entity

# out_line_form = dictionary for formating output lines of catalog &
# names of variables; see lsd_cat_lib for detailed describtion.
out_line_form = lsd_cat_lib.default_out_line_form

# command line parsing with argparse
parser = argparse.ArgumentParser(
    description="""
lsd_cat.py - Create a catalog of sources above a detection
             threshold in 3D datacube containing signal and
             variance (presumably a template cross-correlated
             flux & variance cube from which detection signifcances
             can be calculated).
""",
    epilog="""
Supported values for --tabvalues option:
I - Running ID 
ID - Grouped object ID of an entry in the catalog.
X_PEAK_SN,Y_PEAK_SN, Z_PEAK_SN - Postion of maximum SN.
LAMBDA_PEAK_SN - same as LAMBDA_0, but for Z_PEAK_SN.
RA_PEAK_SN, DEC_PEAK_SN - RA & DEC for X_PEAK_SN, Y_PEAK_SN.

NPIX - Number of voxels above detection threshold per source.
DETSN_MAX - Formal Detection significance of the object
            (i.e. maximum S/N value of the voxels in the obj.).
BORDER - 1 if objects near the field of view border, 0 otherwise.
        (Note: Distance to border is set via --borderdist parameter 
        [default: 10 px])
        (Note: Distances are calculated wrt. to X_PEAK_SN & Y_PEAK_SN)

NOTE: 
By default all pixel coordinates in the output catalog are 1-indexed
(this behaviour can be changed with the --zeroidx switch)
                                
""",
    formatter_class=argparse.RawDescriptionHelpFormatter,
)

parser.add_argument(
    "-i",
    "--input",
    required=True,
    type=str,
    help="""
Name of the input FITS file containing either detection
significance or matched filtered data and propagated variances.
                    """,
)
parser.add_argument(
    "-S",
    "--SHDU",
    type=int_or_str,
    default="DATA_3DCC",
    help=""" 
HDU number (0-indexed) or name of input S/N or filtered cube FITS
file that contains the detection significances or matched filtered
data. If no `--NHDU` (see below) is supplied, we assume that the input
cube in this HDU is S/N, otherwise we assume that it contains the
matched filtered data. [Default: 0] """,
)
parser.add_argument(
    "-N",
    "--NHDU",
    type=int_or_str,
    default=None,
    help=""" 
HDU number (0-indexed) or name of filtered cube FITS file that
contains the propagated variances of the matched filtering
operation. [Default: not set - in this case the datacube in `--SHDU`
is interpreted as S/N] """,
)
parser.add_argument(
    "-e",
    "--expmap",
    type=str,
    default=None,
    help=""" 
FITS file containing a 2D array where the number of exposed voxels
are stored for each spaxel.  The tool `fov_map_from_expcube.py` can
create such a map.  This exposure map is required if the output
parameter `BORDER` is demanded in the `--tabvalues` option (see
below). [Default: None] """,
)
parser.add_argument(
    "-EHDU",
    "--expmaphdu",
    type=int_or_str,
    default="EXP",
    help="""
HDU of exposure map in --expmap. (Default: 'EXP')
""",
)
parser.add_argument(
    "-t",
    "--thresh",
    default=8.0,
    type=float,
    help="""
Detection threshold [Default: 8.0]
                    """,
)
parser.add_argument(
    "-c",
    "--catalogue",
    type=str,
    default=None,
    help=""" 
Filename of the output ASCII catalogue. Two files will be written
on disc, an ASCII catalogue with the specified filename, and an FITS
table with `.fits` being appended to this filename. [Default:
`catalog_+INPUT+.cat`, where `INPUT` is the name of the input FITS
file.]  """,
)
parser.add_argument(
    "--tabvalues",
    type=str,
    default=None,
    help=""" 
Comma-separated list of columns to be written in output
catalogue. See below for a list of supported values. [Default:
`I,ID,X_PEAK_SN,Y_PEAK_SN,Z_PEAK_SN,DETSN_MAX`] """,
)
parser.add_argument(
    "-r",
    "--radius",
    type=float,
    default=0.8,
    help=""" 
Grouping radius in arcsec.  Detections at similar spatial
positions within this search radius get assigned the same
`ID`. [Default: 0.8 arcsec] """,
)
parser.add_argument(
    "--spaxscale",
    type=float,
    default=0.2,
    help=""" 
Plate scale (spatial extent of a pixel) in arcsec. 
[Default: 0.2 arcsec]
                    """,
)
parser.add_argument(
    "--borderdist",
    type=float,
    default=10.0,
    help=""" 
Flag detection in catalogue (column `BORDER`) if it is less than
`BORDERDIST` pixels near the field of view border. Only has an effect
if the field `BORDER` is requested in `--tabvalues` and if an
`--expmap` is supplied. [Default: 10]
                    """,
)
parser.add_argument(
    "--zeroidx",
    action="store_true",
    help="""
Switch to zero-indexed output voxel coordinates. 
(was the default in LSDCat versions < 2).
""",
)
parser.add_argument(
    "--segcube",
    type=str,
    default=None,
    help=""" Filename of segmentation cube.  
(Default: None, i.e. no segmenation cube will be written to disk) """,
)
parser.add_argument(
    "--overwrite",
    action="store_true",
    help=""" 
Overwrite already existing output file!  USE WITH CAUTION AS THIS MAY
OVERWRITE YOUR RESULTS! 
                    """,
)

# parsing arguments and setting variables
args = parser.parse_args()

inputfile = args.input
SHDU = args.SHDU
NHDU = args.NHDU

if args.catalogue == None:
    output_catalog = "catalogue_" + inputfile + ".cat"
else:
    output_catalog = args.catalogue

output_catalog_fits = lsd_cat_lib.fits_cat_name(output_catalog)

if os.path.isfile(output_catalog) and args.overwrite == False:
    print(
        "ERROR: output file "
        + output_catalog
        + " already exists!"
        + " Aborting! Use --overwrite switch to force overwrite."
    )
    sys.exit(2)

radius = args.radius  # grouping radius
thresh = args.thresh  # detection threshold
spaxscale = args.spaxscale  # plate scale

if args.tabvalues == None:
    tabvalues = ["ID", "X_PEAK_SN", "Y_PEAK_SN", "Z_PEAK_SN", "NPIX", "DETSN_MAX"]
else:
    tabvalues = args.tabvalues.split(",")

# test if all entries in --tabvalues are recognised
lsd_cat_lib.tabvalue_test(tabvalues, out_line_form)

# 'BORDER' in tabvalues, but no expmap file given
if "BORDER" in tabvalues and args.expmap == None:
    print(
        "ERROR: 'BORDER' requested in --tabvalues, but no --expmap"
        + " supplied. ABORTING!"
    )
    sys.exit(2)

# X_PEAK_SN, Y_PEAK_SN, Z_PEAK_SN need to be present, otherwise
# lsd_cat_measure.py will not work...
if not [True, True, True] == [
    True for element in ["X_PEAK_SN", "Y_PEAK_SN", "Z_PEAK_SN"] if element in tabvalues
]:
    print(
        inputfile
        + ": WARNING - X_PEAK_SN,Y_PEAK_SN,Z_PEAK_SN not given in "
        + "--tabvalueslsd_cat_measure.py will not work properly on this "
        + "catalog!"
    )

# starting message
starttime = time.time()
now = datetime.now()

print(" ... LSDCat Version " + str(__version__) + " ... ")
print(cats[0])  # :-)

# reading in datacube
print(
    inputfile
    + ": Opening input datacube ... (HDU "
    + str(SHDU)
    + ") "
    + get_timestring(starttime)
)

det_sn_cube, shdu_header = read_hdu(inputfile, SHDU, memmap=False, nans_to_value=True)
primary_header = fits.getheader(inputfile, 0)
wavel_unit = shdu_header["CUNIT3"]
wavel = lsd_cat_lib.wavel_auto(shdu_header)

if NHDU != None:
    # in this case we calculate det_sn_cube on the fly
    print(
        inputfile
        + ": Openening propagated variances "
        + "(HDU "
        + str(NHDU)
        + ") "
        + get_timestring(starttime)
    )
    variance_cube, var_header = read_hdu(
        inputfile, NHDU, memmap=False, nans_to_value=False
    )
    det_sn_cube = det_sn_cube / np.sqrt(variance_cube)

# ACTUAL DETECTION
# .... using scipy.measurements (label & find_objects) ...
print(
    inputfile
    + ": Detecting lines with SN_det ≥ "
    + str(thresh)
    + " ... "
    + get_timestring(starttime)
)

labels = measurements.label(
    det_sn_cube > thresh, structure=None
)  # 6-connected topology
label_cube = labels[0]  # segmentation cube
max_label = labels[1]  # number of detected emissin lines
objects = measurements.find_objects(label_cube)  # slices of minimum
# parallelepipeds
# encompassing
# segments
ids = np.asarray(range(1, max_label + 1))  # all labels of detections

# all information for head of the output catalog is now present...
print(
    inputfile
    + ": There are %d detections above a " % (max_label)
    + "detection threshold of %2.2f " % (thresh)
    + get_timestring(starttime)
)

ascii_cat_head = lsd_cat_lib.ascii_catalog_header(
    primary_header,
    inputfile,
    SHDU,
    NHDU,
    thresh,
    len(ids),
    radius,
    command,
    tabvalues,
    out_line_form,
)
if max_label == 0:
    print(
        inputfile
        + ": No objects to analyse. Empty catalog "
        + "written to disk: "
        + output_catalog
        + " - Quitting now gracefully!"
    )
    # write empty ascii table
    lsd_cat_lib.write_ascii_cat(output_catalog, ascii_cat_head)
    # write empty fits table
    tbhdu = lsd_cat_lib.make_fits_cat(
        primary_header,
        tabvalues,
        [None] * len(tabvalues),
        [out_line_form[entry][2] for entry in tabvalues],
        [out_line_form[entry][3] for entry in tabvalues],
    )
    tbhdu.writeto(output_catalog_fits, overwrite=args.overwrite)
    print(cats[1])
    sys.exit(0)

# ... BASCI MEASUREMENTS on detected emission lines ...
# (those which are requested with the --tabvalues option)

# number of voxels per object
if "NPIX" in tabvalues:
    print(
        inputfile
        + ": Calculating number of voxels per "
        + "object above the detection threshold ... "
        + get_timestring(starttime)
    )
    npix_ids, npix = lsd_cat_lib.calc_npix(label_cube, objects)


# S/N MAX
print(
    inputfile
    + ": Calculating SN extrema & coordinates ... "
    + get_timestring(starttime)
)
det_sn_max, x_sn_max, y_sn_max, z_sn_max = lsd_cat_lib.calc_maxima(
    det_sn_cube, label_cube, objects
)

# RA / DEC / LAMBDA corresponding to S/N MAX (even if not requeted,
# computation is cheap)
lambda_sn_max = lsd_cat_lib.calc_lambda(z_sn_max, shdu_header)
ra_sn_max, dec_sn_max = lsd_cat_lib.pix_to_radec(x_sn_max, y_sn_max, shdu_header)


# dermination of border flag (dev 1.0.2dev):
if "BORDER" in tabvalues:
    border_dist = args.borderdist
    print(
        inputfile
        + ": Flagging Objects "
        + str(border_dist)
        + "px "
        + "near the field of view border using exposure count map "
        + args.expmap
        + "... "
        + get_timestring(starttime)
    )
    expmap_hdu = fits.open(args.expmap)
    expmap = expmap_hdu[args.expmaphdu].data
    if expmap.shape != det_sn_cube[0, :, :].shape:
        print(
            "ERROR: Exposure map dimensions ("
            + str(expmap.shape)
            + ") not compatible with cube "
            + "dimensions ("
            + str(det_sn_cube[0, :, :].shape)
            + ")"
        )
        sys.exit(2)

    border_bit = lsd_cat_lib.calc_border_flag(border_dist, expmap, x_sn_max, y_sn_max)

    print(
        inputfile
        + ": There are "
        + str(np.sum(border_bit))
        + " detections close to the border..."
        + get_timestring(starttime)
    )


# spatial grouping
print(
    inputfile
    + ": Spatially group objects (e.g. assign same "
    + "ID to objects  within "
    + str(radius)
    + " arcseconds) "
    + get_timestring(starttime)
)

ids, index_sort = lsd_cat_lib.group_spatial(
    x_sn_max, y_sn_max, z_sn_max, ids, radius, spaxscale
)

print(
    inputfile + ": The catalog contains " + str(ids.max()) + " individual objects ..."
)

# generate running IDs
if "I" not in tabvalues:
    tabvalues = ["I"] + tabvalues  # we always create running IDs
running_id_order = np.arange(1, len(ids) + 1, 1)
running_ids = np.zeros_like(running_id_order)
running_ids[index_sort] = running_id_order

# ..... WRITING ENTRIES INTO ASCII OUTPUT CATALOG ......
print(
    inputfile
    + ": Starting to write entries into "
    + output_catalog
    + " ASCII output catalog: "
    + get_timestring(starttime)
)
# Note that here also +1. is added to the coordinates, if not
# explicitly set to 0-indexed coordinates (args.zeroidx).  This also
# propagates into the FITS table below, i.e. there the coordinates are 1-indexed
# as well if args.zeroidx == False (the default).
line_string, var_list, unit_list, fits_format_list = lsd_cat_lib.gen_line_string(
    tabvalues, vars(), index_sort=index_sort, zeroidx=args.zeroidx
)

ascii_catalog_lines = lsd_cat_lib.ascii_catalog_lines(ids, var_list, line_string)

lsd_cat_lib.write_ascii_cat(output_catalog, ascii_cat_head + ascii_catalog_lines)

print(
    inputfile
    + ": Wrote output catalog: "
    + output_catalog
    + " "
    + get_timestring(starttime)
)

# ..... WRITING FITS TABLE ....
primary_header = search_header(
    os.path.basename(sys.argv[0]),
    __version__,
    inputfile,
    SHDU,
    NHDU,
    args.expmap,
    args.expmaphdu,
    thresh,
    args.tabvalues,
    radius,
    spaxscale,
    args.borderdist,
    args.zeroidx,
    args.segcube,
    primary_header,
)
primary_header["HISTORY"] = (
    "Created by LSDCat lsd_cat_search.py " + " -- " + now.strftime("%m/%d/%Y, %H:%M:%S")
)
primary_header["HISTORY"] = "--- start of lsd_cat_search.py command ---"
primary_header["HISTORY"] = command
primary_header["HISTORY"] = "--- end of lsd_cat_search.py command ---"
tbhdu = lsd_cat_lib.make_fits_cat(
    primary_header, tabvalues, var_list, unit_list, fits_format_list
)

tbhdu.writeto(output_catalog_fits, overwrite=args.overwrite)

print(
    inputfile
    + ": Wrote FITS catalog "
    + output_catalog_fits
    + " "
    + get_timestring(starttime)
)

if args.segcube != None:
    print(
        inputfile
        + ": Writing segmentation cube "
        + args.segcube
        + " to disk ..."
        + get_timestring(starttime)
    )
    out_wcs = WCS(shdu_header)
    out_header = out_wcs.to_header()
    out_header["EXTNAME"] = "SEGCUBE"
    seg_cube_hdu = fits.ImageHDU(data=label_cube.astype(np.int16), header=out_header)
    primary_hdu = fits.PrimaryHDU(data=None, header=primary_header)
    label_cube_hdu_list = fits.HDUList(hdus=[primary_hdu, seg_cube_hdu])
    label_cube_hdu_list.writeto(args.segcube, overwrite=args.overwrite)
    print(inputfile + ": Done! " + get_timestring(starttime))

print(cats[2])  # ;-)
