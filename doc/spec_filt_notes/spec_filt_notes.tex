\documentclass[a4paper]{article}
\usepackage[a4paper]{geometry}
\usepackage{amsmath,amssymb,wasysym,gensymb,marvosym,graphicx}

\title{A Vectorized Algorithm for Spectral Convolution}
\author{Edmund Christian Herenz
	}

\date{\today}

\begin{document}

\maketitle

\section{Introduction}
\label{sec:introduction}

The spectral cross-corellation in \texttt{LSDCat} is implemented as a
vectorized convolution method similar to Das 1991 (Sect. 3.3.6).  The
aim of this short manuscript is to detail the main ideas of the
algorithm used in the \texttt{LSDCat} subroutine
\texttt{lsd\_cc\_spectral.py}.

\section{Preliminaries}
\label{sec:preliminaries}

The convolution of two functions $f$ and $g$ is denoted symbolically
by a $*$:
\begin{equation}
  \label{eq:16}
  [f * g](x) = \int_{-\infty}^{+\infty} f(\xi) g(x-\xi) \, \mathrm{.}
\end{equation}
In signal theory the function $f$ in above definitions is called the
\textit{signal} while $g$ is called the \textit{impulse response}
(i.e. the response of signal processing system to a $\delta(x)$
impulse).  Here the terms ``spectrum'' and ``filter'' seem more
appropriate and will be used.

Discretizing Eq.~(\ref{eq:16}) gives
\begin{equation}
  \label{eq:23}
  h(n) = \sum_{k=-\infty}^{+\infty} f(k) g(n-k) =
  \sum_{k=-\infty}^{+\infty} f(n-k) g(k)\;\text{.}
\end{equation}
Here $h(n)$ is the flux of the convolved spectrum at position $n$.
For the design of the matched filter that is used in LSDCat see Herenz
(2023).  The main difficulty faced here is that the shape of $g$ is
dependent on $n$ -- symbolically $g_n(k)$.


\section{Implementation}
\label{sec:implementation}

Starting from Eq.~(\ref{eq:23}) and asserting a linearly sampled
wavelength grid
\begin{equation}
  \label{eq:1}
  \lambda(z) = \lambda_\text{Start} + z \cdot \Delta \lambda
\end{equation}
the discrete convolution on a finite
grid can be written as
\begin{equation}
  \label{eq:45}
  h(z) = \sum_{k=-M}^{+M} g(k) f(z-k) \;\text{.}
\end{equation}
Here $M$ denotes the width where the filter on the grid needs to
truncated (i.e. $g(z>M) = g(z<M) \equiv 0 \; \forall z$). The total
width of the filter is thus
\begin{equation}
  \label{eq:51}
  M' = 2\cdot M \;\text{.}
\end{equation}
Further without loss of generality $f(z<0)=g(z<0)\equiv 0$ can be
assumed, since the filter and spectrum have discrete length and can
thus be shifted accordingly.  Having considered above remarks
Eq.~(\ref{eq:45}) now writes:
\begin{equation}
  \label{eq:48}
  h(z) = \sum_{k=0}^{2\cdot M} g_z(k) f(z-k) \;\text{.}
\end{equation}
In Eq.~(\ref{eq:48}) the truncation length $M$ has been assumed
independent of $z$, which might not necessarily be the case,
i.e. $M=M(z)$ . In practice however, the sampled Gaussian
\begin{equation}
  \label{eq:34}
  g_{z}(\xi) = \frac{1}{\sqrt{2 \pi} \sigma_{z}}  \exp \left (
    - \frac{\xi^2}{2 \sigma_{z}^2} \right ) 
\end{equation}
with
\begin{equation}
  \label{eq:35}
  \sigma_{z} = \frac{v}{c} \left (\frac{\lambda_\text{Start}}{\Delta \lambda}
    + z\right ) \;\text{,}
\end{equation}
increases in width with increasing wavelength (see also Herenz and
Wisotzki 2017).  We express the truncation length as
\begin{equation}
  \label{eq:49}
  M = M(z_\text{max}) = C \times \sigma_{z_\text{max}} + 1
\end{equation}
with $\sigma_z$ according to Eq.~(\ref{eq:35}) for the spectral
coordinate $z_\text{max}$ corresponding to the longest wavelength
present in the datacube.  The constant $C$ is called tapering constant
and we opted for $C=4$. Numerical experiments verified that larger
tapering constants do not alter the results while increasing the
computational time.


Writing $z_\text{max}=N-1$ a spectrum can  be represented by a column vector
\begin{equation}
  \label{eq:50}
  \mathbf{f} =
  \begin{pmatrix}
    f(0) \; ,\; \cdots \;,\; f(N-1)
  \end{pmatrix}^T \;\text{.}
\end{equation}
Introudcing the $N \times (M'+N)$ matrix 
\begin{equation}
  \label{eq:47}
  \mathbf{G} = \small
  \begin{pmatrix}
    g_0(0) & 0 & 0 & \cdots &\cdots &\cdots& 0 \\
    g_1(1) & g_1(0)&  0 & \ddots & \cdots &\cdots& \vdots \\
    g_2(2) & g_2(1) & g_2(0) & 0 & \ddots &\cdots& \vdots\\
    \vdots & \ddots & \ddots & \ddots &  \cdots &\ddots&\vdots \\ 
    g_{M'}(M')& g_{M'}(M'-1) & \cdots & \ddots & 0 & \cdots& 0 \\
    \\
    0 & g_{M'+1}(M') &  g_{M'+1}(M'-1) & \cdots & \ddots &\ddots &\vdots
    \\ 
    \vdots & \vdots & \vdots & \vdots & \vdots  & \vdots & \vdots \\
    \vdots &\ddots &\vdots &\ddots &\ddots &\ddots &\vdots \\
    \vdots  & \ddots & \ddots & 0 & g_{N-1}(M') & \cdots & g_{N-1}(0)\\ 
    \vdots & \ddots & \ddots & \ddots & \ddots & \ddots & \vdots \\
    0 & \cdots & \cdots & \cdots & \cdots & 0 & g_{M'+N-1}(M')
     \end{pmatrix} \normalsize \;\text{,}
\end{equation}
where the components $g_n$ represent the filter of Eq.~(\ref{eq:48})
and multiplying this matrix with $\mathbf{f}$ of Eq.~(\ref{eq:50})
\begin{equation}
  \label{eq:46}
  \mathbf{h} = \mathbf{G}\cdot\mathbf{f} \;\text{,}
\end{equation} 
results in the $2M'+N$ dimensional column vector
\begin{equation}
  \label{eq:52}
  \mathbf{h} =
  \begin{pmatrix}
    h(0) \;,\; \cdots \;,\; h(M)\;, \cdots \;,\;h(M+N-1)\; ,\; \cdots
    \;,\; h(M'+N-1) \;
  \end{pmatrix}^T \;\text{.}
\end{equation}
Figure~\ref{fig:1} visualises $G$ for the typical dimensions
encountered in MUSE.

Comparing now Eq.~(\ref{eq:52}) with Eq.~(\ref{eq:48}) it can be seen,
that the subset of $N$ ordered elements
\begin{equation}
  \label{eq:54}
  \mathbf{h}'= \big ( h(M)\;, \; \cdots \;,\;h(M+N-1) \big )^T
\end{equation}
of $\mathbf{h}$ now represent the convolved spectrum of
Eq.~(\ref{eq:50}), i.e.
\begin{equation}
  \label{eq:53}
  \mathbf{h}' = \big ( h(0)\; , \; \cdots \; , \;h(N-1) \big )^T
  \;\text{,}
\end{equation}
which are all $h(n)$ on the left hand side of Eq.~(\ref{eq:48}).  We
note that in this formalism values outside of the spectral range are
treated as zeros.

We observe that the matrix $\mathbf{G}$ is a lower-banded tri-diagnoal
matrix (see Figure~\ref{fig:1}), i.e. it is sparse.  This property
allows to use even faster sparse-matrix multiplication algortihms in
\texttt{LSDCat}, in practice we use the
\texttt{scipy.sparse.csr\_matrix} representation (Virtanen et
al. 2020). Furthermore, by representing the data-cube as a list of
ordered spectra the convolution operation is trivially parallelised.

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{matrix_example.pdf}
  \caption{Visualisation of the matrix $\mathbf{G}$ introuced in
    Eq.~(\ref{eq:47}) for the typical MUSE spectral dimension
    ($z_\mathrm{max}=3460$).  The grey area are elements
    $G_{ij} \neq 0$ and the line shows the diagnoal
    $\delta_{ij} = (1 \forall i \equiv j, 0 \forall i \neq j)$. For
    illustrative purposes we set here $v=20000$\,km/s in Eq.~(\ref{eq:35});
    for typical velocities in practical applications the
    $G_{ij} \neq 0$ band is significantly narrower. }
  \label{fig:1}
\end{figure}

\subsubsection*{References}

Das, P.K. (1991). \emph{Optical Signal Processing -
  Fundamentals}. Springer, Berlin, Heidelberg, New York.
\\
Herenz, E.C. 2023, Astronomical Notes, Vol. 344 (5), e20220091 
\\
Virtanen, P., Gommers, R., Oliphant, T.E. et al. SciPy 1.0:
fundamental algorithms for scientific computing in Python. Nat Methods
17, 261–272 (2020)

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "british"
%%% End:
